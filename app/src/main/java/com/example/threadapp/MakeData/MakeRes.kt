package com.example.threadapp.MakeData

data class MakeRes(
    val data: DataCreate,
    val message: String,
    val status: Int

)


data class DataCreate(
    val text: String,
    val createdAt: String
)