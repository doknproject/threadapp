package com.example.threadapp.SignUpData

data class User(
    val bio: Any,
    val birthday: Any,
    val email: String,
    val firstName: String,
    val id: String,
    val lastName: String,
    val privateAccount: Boolean,
    val profileImage: String,
    val username: String,
    val verified: Boolean
)