package com.example.threadapp.SignUpData

data class registerRes(
    val access_token: String,
    val message: String,
    val status: Int,
    val user: User
)