package com.example.threadapp.SignUpData

data class registerReq(
    val email : String,
    val firstName : String,
    val lastName : String,
    val password : String,
    val username : String
)