package com.example.threadapp.viewmodel

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.threadapp.LogInData.SignInReq
import com.example.threadapp.LogInData.SignInRes
import com.example.threadapp.repository.LocalRepository
import com.example.threadapp.service.LocalRetrofitInstance
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.Response



class LoginViewModel : ViewModel() {
    private val repository = LocalRepository()

    private val _login = MutableLiveData<Response<SignInRes>?>()
    val login: MutableLiveData<Response<SignInRes>?> = _login

    val _errorMessage = MutableLiveData<String?>()
    val errorMessage: MutableLiveData<String?> = _errorMessage

    val loading =  mutableStateOf(false)


    fun login(loginReq: SignInReq) {
        viewModelScope.launch {
            try {
                loading.value = true
                delay(500)
                val loginRes = repository.login(loginReq)
                Log.d("ali",loginRes.toString())
                _login.value = loginRes
            } catch (e: Exception) {
                _errorMessage.value = "Error: $e"
            } finally {
                loading.value = false
            }
        }
    }
    fun clearResponse() {
        _login.value = null
    }
}