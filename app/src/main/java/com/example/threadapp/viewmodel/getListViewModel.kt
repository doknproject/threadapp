package com.example.threadapp.viewmodel

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.example.threadapp.MakeListData.MakeListDataRes
import com.example.threadapp.SignUpData.checkUserNameReq
import com.example.threadapp.SignUpData.checkUserNameRes
import com.example.threadapp.profileData.ProfileData
import com.example.threadapp.repository.LocalRepository
import com.example.threadapp.service.LocalRetrofitInstance
import com.example.threadapp.service.LocalService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class GetListViewModel: ViewModel(){
    private val repository = LocalRepository()

    private val _getlist = MutableLiveData<MakeListDataRes>()
    val getlist: LiveData<MakeListDataRes> = _getlist

    val loading =  mutableStateOf(false)

    fun fetchDataList(token: String) {
        viewModelScope.launch {
            try {
                loading.value = true
                val threadData = repository.getDataList(token)
                _getlist.value = threadData
            }
            catch (e: Exception){
                Log.d("repo", "fetchDataList: ${e.message}")
            }
            loading.value = false

        }
    }

}


/*
class GetListViewModel1 : ViewModel() {
    private val repository = LocalRepository()

    // LiveData for the check username response
    private val _getlist = MutableLiveData<Response<MakeListDataRes>>()
    val getlist: LiveData<Response<MakeListDataRes>> get() = _getlist

    val loading = mutableStateOf(false)

    // Function to check username
    fun getDataList(token: String) {
        viewModelScope.launch {
            try {
                Log.d("A_K", "getDataList: " + token)
                loading.value = true
                val response = repository.getDataList(token)
                _getlist.value = response
                Log.e("profile success", _getlist.value.toString())
            } catch (e: Exception) {
                // Handle error
                Log.e("profile", "Error: ${e.message}")
            }
            loading.value = false
        }
    }
}
*/


/*class GetListViewModel : ViewModel() {
    private val repository = LocalRepository()

    // LiveData for the check username response
    private val _getlist = MutableLiveData<Response<MakeListDataRes>>()
    val getlist: MutableLiveData<Response<MakeListDataRes>> = _getlist

    val loading = mutableStateOf(false)

    // Function to check username
    fun getDataList(token:String) {
        viewModelScope.launch {
            try {
                loading.value = true
                val response = repository.getDataList(token)
                _getlist.value = response
                Log.e("profile sucess", _getlist.value.toString())
            } catch (e: Exception) {
                // Handle error
                Log.e("profile", e.message.toString())
            }
            loading.value = false
        }
    }
}*/
/*
class GetListViewModel : ViewModel() {

    // Repository is not used directly in Retrofit-based ViewModel
    // private val repository = LocalRepository()

    // LiveData for the response from the API
    private val _getlist = MutableLiveData<Response<MakeListDataRes>>()
    val getlist: LiveData<Response<MakeListDataRes>> = _getlist

    // State for loading indicator
    val loading = mutableStateOf(false)

    // Function to fetch data using Retrofit
    fun fetchData(token: String) {
        viewModelScope.launch {
            try {
                loading.value = true
                // Call the API using Retrofit instance
                val response = LocalRetrofitInstance.localService.getDataList("Bearer $token")
                _getlist.value = response
                Log.e("getlist success", response.toString())
            } catch (e: Exception) {
                // Handle error
                Log.e("getlist error", e.message.toString())
            } finally {
                loading.value = false
            }
        }
    }
}
*/
