package com.example.threadapp.viewmodel

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.threadapp.MakeData.MakeReq
import com.example.threadapp.MakeData.MakeRes
import com.example.threadapp.SignUpData.checkEmailReq
import com.example.threadapp.SignUpData.checkEmailRes
import com.example.threadapp.SignUpData.checkUserNameReq
import com.example.threadapp.SignUpData.checkUserNameRes
import com.example.threadapp.ThreadData.ThreadDataReq
import com.example.threadapp.ThreadData.ThreadDataRes
import com.example.threadapp.repository.LocalRepository
import kotlinx.coroutines.launch
import retrofit2.Response





class MakeThreadViewModel : ViewModel() {
    private val repository = LocalRepository()

    private val _makethread = MutableLiveData<Response<MakeRes>?>()
    val makethread: MutableLiveData<Response<MakeRes>?> = _makethread

    val loading =  mutableStateOf(false)


    fun makethread(token:String,MakeReq: MakeReq) {
        viewModelScope.launch {

            try {
                loading.value = true
                val MakeRes = repository.Create(MakeReq,token)
                _makethread.value = MakeRes
                Log.e("MakeThread", _makethread.value.toString())
            } catch (e: Exception) {
                // Handle error
                Log.e("MakeThread", e.message.toString())
            }
            loading.value = false
        }
    }

    fun clearResponse() {
        _makethread.value = null
    }
}
