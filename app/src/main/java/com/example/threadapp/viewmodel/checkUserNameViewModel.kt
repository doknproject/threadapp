package com.example.threadapp.viewmodel

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.threadapp.SignUpData.checkEmailReq
import com.example.threadapp.SignUpData.checkEmailRes
import com.example.threadapp.SignUpData.checkUserNameReq
import com.example.threadapp.SignUpData.checkUserNameRes
import com.example.threadapp.repository.LocalRepository
import kotlinx.coroutines.launch
import retrofit2.Response


class CheckUserNameViewModel : ViewModel() {
    private val repository = LocalRepository()

    // LiveData for the check username response
    private val _checkusername = MutableLiveData<Response<checkUserNameRes>?>()
    val checkusername: MutableLiveData<Response<checkUserNameRes>?> = _checkusername

    val loading = mutableStateOf(false)

    // Function to check username
    fun checkusername(checkUserNameReq: checkUserNameReq) {
        viewModelScope.launch {
            try {
                loading.value = true
                val checkUserNameRes = repository.checkUserNameReq(checkUserNameReq)
                _checkusername.value = checkUserNameRes
                Log.e("CheckUserName", _checkusername.value.toString())
            } catch (e: Exception) {
                // Handle error
                Log.e("CheckUserName", e.message.toString())
            }
            loading.value = false
        }
    }
}


/*
class checkUserNameViewModel : ViewModel(){
    private val repository = LocalRepository()

    private  val _checkusername = MutableLiveData<checkEmailRes>()
    val  checkusername : LiveData<checkEmailRes> = _checkusername

    val _errorMessage = MutableLiveData<String?>()
    val errorMessage: MutableLiveData<String?> = _errorMessage

    fun checkusername(checkUserNameReq: checkUserNameReq){
        viewModelScope.launch {

            try {
                val checkusername = repository.checkEmail(checkUserNameReq)
                _checkusername.value = checkusername
                Log.e("Login",_checkusername.value.toString())

                when (checkusername.statusCode) {
                    in 200..299 -> {
                        _errorMessage.value = null
                    }
                    */
/*     401 -> {
                             _errorMessage.value = "Entering wrong username or password"
                         }
                         404 -> {
                             _errorMessage.value = "Username not found"
                         }
                         in 500..599 ->
                             _errorMessage.value = "error on range of 500"

                         else -> {
                             _errorMessage.value = "An unexpected error occurred"
                         }*//*

                }
            } catch (e: Exception) {
                // Handle error
                Log.e("Login", "username not found: ${e.message}")
                _errorMessage.value = e.message ?: "An error occurred"
            }
        }

    }
}*/
