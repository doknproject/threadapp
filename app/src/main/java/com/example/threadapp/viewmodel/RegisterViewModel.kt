package com.example.threadapp.viewmodel

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.threadapp.SignUpData.checkUserNameReq
import com.example.threadapp.SignUpData.checkUserNameRes
import com.example.threadapp.SignUpData.registerReq
import com.example.threadapp.SignUpData.registerRes
import com.example.threadapp.repository.LocalRepository
import kotlinx.coroutines.launch
import retrofit2.Response


class RegisterViewModel : ViewModel() {
    private val repository = LocalRepository()

    // LiveData for the check username response
    private val _register = MutableLiveData<Response<registerRes>?>()
    val register: MutableLiveData<Response<registerRes>?> = _register

    val loading = mutableStateOf(false)

    // Function to check username
    fun register(registerReq: registerReq) {
        viewModelScope.launch {
            try {
                loading.value = true
                val registerRes = repository.register(registerReq)
                _register.value = registerRes
                Log.e("CheckUserName", _register.value.toString())
            } catch (e: Exception) {
                // Handle error
                Log.e("CheckUserName", e.message.toString())
            }
            loading.value = false
        }
    }
}