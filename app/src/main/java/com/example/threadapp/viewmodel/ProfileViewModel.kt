package com.example.threadapp.viewmodel

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.threadapp.MakeListData.MakeListDataRes
import com.example.threadapp.profileData.ProfileData
import com.example.threadapp.repository.LocalRepository
import kotlinx.coroutines.launch
import retrofit2.Response



class ProfileViewModel : ViewModel() {
    private val repository = LocalRepository()

    // LiveData for the check username response
    private val _profileviewmodel = MutableLiveData<Response<ProfileData>>()
    val profile: MutableLiveData<Response<ProfileData>> = _profileviewmodel

    val loading = mutableStateOf(false)

    // Function to check username
    fun profile(token:String) {
        viewModelScope.launch {
            try {
                loading.value = true
                val response = repository.profileData(token)
                _profileviewmodel.value = response
                Log.e("profile sucess", _profileviewmodel.value.toString())
            } catch (e: Exception) {
                // Handle error
                Log.e("profile", e.message.toString())
            }
            loading.value = false
        }
    }
}
