package com.example.threadapp.viewmodel

import android.util.Log
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.threadapp.LogInData.SignInReq
import com.example.threadapp.LogInData.SignInRes
import com.example.threadapp.SignUpData.checkEmailReq
import com.example.threadapp.SignUpData.checkEmailRes
import com.example.threadapp.repository.LocalRepository
import kotlinx.coroutines.launch
import retrofit2.Response

/*

class CheckEmailViewModel : ViewModel(){
    private val repository = LocalRepository()

    private  val _checkemail = MutableLiveData<checkEmailRes>()
    val  checkemail : LiveData<checkEmailRes> = _checkemail

    val _errorMessage = MutableLiveData<String?>()
    val errorMessage: MutableLiveData<String?> = _errorMessage

    fun checkemail(checkEmailReq: checkEmailReq){
        viewModelScope.launch {

            try {
                val checkEmailRes = repository.checkEmail(checkEmailReq)
                _checkemail.value = checkEmailRes
                Log.e("Login",_checkemail.value.toString())

                when (checkEmailRes.statusCode) {
                    in 200..299 -> {
                        _errorMessage.value = null
                    }
               */
/*     401 -> {
                        _errorMessage.value = "Entering wrong username or password"
                    }
                    404 -> {
                        _errorMessage.value = "Username not found"
                    }
                    in 500..599 ->
                        _errorMessage.value = "error on range of 500"


                    else -> {
                        _errorMessage.value = "An unexpected error occurred"
                    }*//*

                }
            } catch (e: Exception) {
                // Handle error
                Log.e("Login", "username not found: ${e.message}")
                _errorMessage.value = e.message ?: "An error occurred"
            }
            }

        }
    }

*/


class CheckEmailViewModel : ViewModel() {
    private val repository = LocalRepository()

    private val _checkemail = MutableLiveData<Response<checkEmailRes>?>()
    val checkemail: MutableLiveData<Response<checkEmailRes>?> = _checkemail

    val loading =  mutableStateOf(false)


    fun checkemail(checkEmailReq: checkEmailReq) {
        viewModelScope.launch {
            loading.value = true
            try {
                val checkEmailRes = repository.checkEmail(checkEmailReq)
                _checkemail.value = checkEmailRes
                Log.e("CheckEmail", _checkemail.value.toString())
            } catch (e: Exception) {
                // Handle error
                Log.e("CheckEmail", e.message.toString())
            }
            loading.value = false
        }
    }

    fun clearResponse() {
        _checkemail.value = null
    }
}

