package com.example.threadapp.MakeListData

data class Data(
    val createdAt: String,
    val id: String,
    val text: String,
    val user: User
)