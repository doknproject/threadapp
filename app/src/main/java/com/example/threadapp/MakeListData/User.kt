package com.example.threadapp.MakeListData

    data class User(
        val bio: String,
        val birthday: Any,
        val email: String,
        val firstName: String,
        val id: String,
        val lastName: String,
        val privateAccount: Boolean,
        val profileImage: String,
        val username: String,
        val verified: Boolean
    )