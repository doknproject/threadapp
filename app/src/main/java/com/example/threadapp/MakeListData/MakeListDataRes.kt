package com.example.threadapp.MakeListData

data class MakeListDataRes(
    val `data`: List<Data>,
    val status: Int
)
