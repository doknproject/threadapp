package com.example.threadapp.Compose

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.threadapp.R

@Composable
fun Editprofile(modifier: Modifier = Modifier, navController: NavController) {

    Column(Modifier.fillMaxSize().padding(top = 10.dp)) {

        Row (
            Modifier.fillMaxWidth().padding( start = 10.dp, end = 10.dp ),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        )
        {
            Row(
                verticalAlignment = Alignment.CenterVertically

            ) {
                IconButton(onClick = {navController.navigate("ProfileScreen") }) {
                    Icon(
                        painter = painterResource(id = R.drawable.close_lg),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onPrimary,
                        modifier = Modifier .size(18.dp)
                    )
                }

                Spacer(Modifier.width(10.dp))
                Text(
                    text = " Edit Profile ",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontSize = 17.sp,
                    modifier = Modifier.clickable {  }
                )


            }

            Text(
                text = " Done ",
                color = MaterialTheme.colorScheme.onPrimary,
                fontSize = 16.sp,
                modifier = Modifier.clickable {  }
            )



        }

        Box(
            modifier.systemBarsPadding()
                .fillMaxSize(),
            Alignment.Center
        )
        {
            Box(

            ) {


            }



        }

    }



}