package com.example.threadapp.Compose

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.threadapp.Function.Follower
import com.example.threadapp.R

@Composable
fun SearchScreen(navController: NavController) {

    var search by remember {
        mutableStateOf("")
    }

   Column(Modifier.fillMaxSize()) {

       Column(
           Modifier
               .systemBarsPadding()
               .fillMaxSize()
               .background(MaterialTheme.colorScheme.background)
               .padding(start = 10.dp, end = 10.dp, top = 25.dp),

           )
       {
           Text(text = "Search", fontSize = 32.sp, color = MaterialTheme.colorScheme.onPrimary)
           
           Spacer(modifier = Modifier.height(10.dp))

           Row() {
               TextField(
                   value = search ,
                   onValueChange = { newtext -> search = newtext },
                   modifier = Modifier
                       .fillMaxWidth()
                       .height(60.dp)
                       .border(
                           width = 1.dp,
                           color = Color(0xFF312E2E),
                           shape = RoundedCornerShape(10.dp)
                       ),
                   label = {
                       Text(
                           text = "Search",
                           color = Color(0xFF878787),
                           fontSize = 14.sp
                       )
                   },
                   leadingIcon = {
                       Icon(
                           imageVector = ImageVector.vectorResource(id = R.drawable.search),
                           contentDescription = null,
                           Modifier.size(24.dp),
                           tint = Color(0xFFA0A0A0)
                       )
                   },
                   singleLine = true,
                   maxLines = 1,
                   colors = TextFieldDefaults.colors(
                       unfocusedIndicatorColor = Color.Transparent,
                       focusedIndicatorColor = Color.Transparent,
                       focusedContainerColor = Color.Transparent,
                       unfocusedContainerColor = Color.Transparent,
                       focusedLabelColor = Color.Black,
                       unfocusedLabelColor = Color.Black,
                       focusedTextColor = MaterialTheme.colorScheme.onPrimary,
                       unfocusedTextColor = MaterialTheme.colorScheme.onPrimary,
                       cursorColor = MaterialTheme.colorScheme.onPrimary
                   ),
                   shape = RoundedCornerShape(10.dp)
               )
           }

           Spacer(modifier = Modifier.height(20.dp))

           Follower()

       }



   }


}