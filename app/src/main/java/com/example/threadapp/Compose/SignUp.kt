package com.example.threadapp.Compose

import android.widget.Toast
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController
import com.example.threadapp.R
import com.example.threadapp.SignUpData.checkEmailReq
import com.example.threadapp.SignUpData.checkUserNameReq
import com.example.threadapp.SignUpData.registerReq
import com.example.threadapp.ui.theme.gwendoly
import com.example.threadapp.viewmodel.CheckEmailViewModel
import com.example.threadapp.viewmodel.CheckUserNameViewModel
import com.example.threadapp.viewmodel.RegisterViewModel

@Composable
fun SignUp(
    currentState: Int,
    onNext: () -> Unit,
    onPrevious: () -> Unit,
    navController: NavController,

) {
    var checkemail by remember { mutableStateOf("") }
    var username by remember { mutableStateOf("") }
    var name by remember { mutableStateOf("") }
    var lastname by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    var againpass by remember { mutableStateOf("") }


    var passvisible by remember { mutableStateOf(false) }
    var againpassvisible by remember { mutableStateOf(false) }






    val checkEmailViewModel = viewModel<CheckEmailViewModel>()
    val checkUsernameViewModel = viewModel<CheckUserNameViewModel>()
    val RegisterViewModel = viewModel<RegisterViewModel>()

    //loading!
    val checkEmailloading = checkEmailViewModel.loading.value
    val checkUserNameloading = checkUsernameViewModel.loading.value
    val registerloading = RegisterViewModel.loading.value


    // Observing responses
    val checkEmailResponse by checkEmailViewModel.checkemail.observeAsState()
    val checkUsernameResponse by checkUsernameViewModel.checkusername.observeAsState()
    val RegisterResponse by RegisterViewModel.register.observeAsState()


    val triggerNext by rememberUpdatedState(onNext)
    val ctx = LocalContext.current

    // Handling email response
    LaunchedEffect(checkEmailResponse) {
        checkEmailResponse?.let { res ->
            if (res.isSuccessful) {
                val responseBody = res.body()
                when (responseBody?.statusCode) {
                    in 200..299 -> {
                        Toast.makeText(ctx, "success", Toast.LENGTH_SHORT).show()
                        triggerNext() // Trigger next state
                        checkEmailViewModel.clearResponse()
                    }

                    in 500..599 -> {
                        Toast.makeText(ctx, "server error", Toast.LENGTH_SHORT).show()
                    }

                    in 400..499 -> {
                        Toast.makeText(ctx, "the email exist!", Toast.LENGTH_SHORT).show()
                    }

                    else -> {
                        Toast.makeText(ctx, "please try again later", Toast.LENGTH_SHORT).show()
                    }
                }
            } else {
                Toast.makeText(ctx, "please try again later", Toast.LENGTH_SHORT).show()
            }
        }
    }


    // Handling username response
    LaunchedEffect(checkUsernameResponse) {
        checkUsernameResponse?.let { res ->
            if (res.isSuccessful) {
                val responseBody = res.body()
                when (responseBody?.statusCode) {
                    in 200..299 -> {
                        Toast.makeText(ctx, "success", Toast.LENGTH_SHORT).show()
                        triggerNext() // Trigger next state
                        checkEmailViewModel.clearResponse()
                    }

                    in 500..599 -> {
                        Toast.makeText(ctx, "server error", Toast.LENGTH_SHORT).show()
                    }

                    in 400..499 -> {
                        Toast.makeText(ctx, "The userName exist!", Toast.LENGTH_SHORT).show()
                    }

                    else -> {
                        Toast.makeText(ctx, "please try again later", Toast.LENGTH_SHORT).show()
                    }
                }
            } else {
                Toast.makeText(ctx, "please try again later", Toast.LENGTH_SHORT).show()
            }
        }
    }


    LaunchedEffect(RegisterResponse) {
        RegisterResponse?.let { res ->
            if (res.isSuccessful) {
                val responseBody = res.body()
                when (responseBody?.status) {
                    in 200..299 -> {
                        navController.navigate("SignIn")
                    }

                    in 500..599 -> {
                        Toast.makeText(ctx, "server error", Toast.LENGTH_SHORT).show()
                    }

                }
            } else {
                Toast.makeText(ctx, "please try again later", Toast.LENGTH_SHORT).show()
            }
        }
    }

    Column(
        modifier = Modifier.fillMaxSize()
    )
    {
        Spacer(modifier = Modifier.height(15.dp))
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ) {
            IconButton(onClick = { }) {
                Icon(
                    painter = painterResource(id = R.drawable.threads__app__logo),
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.onPrimary,
                    modifier = Modifier .size(72.dp)
                )
            }
        }
        Spacer(modifier = Modifier.height(120.dp))

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center,
        ) {
            Text(
                text = "Threads",
                color = MaterialTheme.colorScheme.onPrimary,
                fontFamily = gwendoly,
                fontSize = 50.sp,
                fontWeight = FontWeight.Normal
            )
        }

        Spacer(modifier = Modifier.height(25.dp))



        when (currentState) {
            0 -> {
                val a = LocalContext.current
                Row(Modifier.padding(start = 5.dp, end = 5.dp)) {
                    TextField(
                        value = checkemail,
                        onValueChange = { newtext -> checkemail = newtext },
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(60.dp)
                            .border(
                                width = 1.dp,
                                color = Color(0xFF312E2E),
                                shape = RoundedCornerShape(5.dp)
                            ),
                        label = {
                            Text(
                                text = "Enter your email",
                                color = Color(0xFF878787),
                                fontSize = 14.sp
                            )
                        },
                        singleLine = true,
                        maxLines = 1,
                        colors = TextFieldDefaults.colors(
                            unfocusedIndicatorColor = Color.Transparent,
                            focusedIndicatorColor = Color.Transparent,
                            focusedContainerColor = Color.Transparent,
                            unfocusedContainerColor = Color.Transparent,
                            focusedLabelColor = Color.Black,
                            unfocusedLabelColor = Color.Black,
                            focusedTextColor = MaterialTheme.colorScheme.onPrimary,
                            unfocusedTextColor = MaterialTheme.colorScheme.onPrimary,
                        ),
                        shape = RoundedCornerShape(5.dp)
                    )
                }
//2
                Spacer(modifier = Modifier.height(16.dp))

                //shoro

                Row(Modifier.padding(start = 5.dp, end = 5.dp)) {
                    val isButtonEnabled = checkemail.isNotEmpty()

                    Button(
                        {
                            if (isButtonEnabled && checkemail.contains("@gmail.com")) {
                                checkEmailViewModel.checkemail(checkEmailReq(checkemail))
                            }
                            else {
                                Toast.makeText(
                                    a,
                                    "Ensure that you use valid Emailaddress ",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(60.dp),
                        //.clip(RoundedCornerShape(5.dp)),
                        colors = ButtonDefaults.buttonColors(
                            containerColor = if (isButtonEnabled) Color(0xFF0070FA) else Color(0xFFB8B8B8)
                        )
                    ) {
                        if (checkEmailloading == true) {
                            CircularProgressIndicator(
                                color = Color(0xFFB8B8B8),
                                modifier = Modifier.size(24.dp)
                            )
                        } else {
                            Row(
                                modifier = Modifier.fillMaxWidth(),
                                horizontalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    text = "Continue",
                                    fontSize = 16.sp,
                                    color = if (isButtonEnabled) Color.White else Color.Gray // Change text color based on state
                                )
                            }
                        }
                    }
                }
                //2
            }

            1 -> {
                val a = LocalContext.current
                Row(Modifier.padding(start = 5.dp, end = 5.dp)) {
                    TextField(
                        value = username,
                        onValueChange = { newtext -> username = newtext },
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(60.dp)
                            .border(
                                width = 1.dp,
                                color = Color(0xFF312E2E),
                                shape = RoundedCornerShape(5.dp)
                            ),
                        label = {
                            Text(
                                text = "Enter your username",
                                color = Color(0xFF878787),
                                fontSize = 14.sp
                            )
                        },
                        singleLine = true,
                        maxLines = 1,
                        colors = TextFieldDefaults.colors(
                            unfocusedIndicatorColor = Color.Transparent,
                            focusedIndicatorColor = Color.Transparent,
                            focusedContainerColor = Color.Transparent,
                            unfocusedContainerColor = Color.Transparent,
                            focusedLabelColor = Color.Black,
                            unfocusedLabelColor = Color.Black,
                            focusedTextColor = MaterialTheme.colorScheme.onPrimary,
                            unfocusedTextColor = MaterialTheme.colorScheme.onPrimary,
                        ),
                        shape = RoundedCornerShape(5.dp)
                    )
                }
                Spacer(modifier = Modifier.height(16.dp))
                Row(Modifier.fillMaxWidth()) {
                    val isButtonEnabled = username.isNotEmpty()
                    Button(
                        {
                            if (username.length >= 1 && isButtonEnabled)
                                checkUsernameViewModel.checkusername(checkUserNameReq(username))
                            else if (username.length < 1) {
                                Toast.makeText(
                                    a,
                                    "Ensure that you use valid userName ",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        },

                        modifier = Modifier
                            .fillMaxWidth()
                            .height(60.dp),
                        //.clip(RoundedCornerShape(5.dp)),
                        colors = ButtonDefaults.buttonColors(
                            containerColor = if (isButtonEnabled) Color(0xFF0070FA) else Color(
                                0xFFB8B8B8
                            )
                        )
                    ) {
                        if (checkUserNameloading == true) {
                            CircularProgressIndicator(
                                color = Color(0xFFB8B8B8),
                                modifier = Modifier.size(24.dp)
                            )
                        } else {
                            Row(
                                modifier = Modifier.fillMaxWidth(),
                                horizontalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    text = "Continue",
                                    fontSize = 16.sp,
                                    color = if (isButtonEnabled) Color.White else Color.Gray
                                )
                            }
                        }
                    }
                }
            }


            2 -> {
                val a = LocalContext.current
                val isButtonEnabled = name.isNotEmpty() && lastname.isNotEmpty()
                Row(Modifier.padding(start = 5.dp, end = 5.dp)) {
                    TextField(
                        value = name,
                        onValueChange = { newtext -> name = newtext },
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(60.dp)
                            .border(
                                width = 1.dp,
                                color = Color(0xFF312E2E),
                                shape = RoundedCornerShape(5.dp)
                            ),
                        label = {
                            Text(
                                text = "Enter your name",
                                color = Color(0xFF878787),
                                fontSize = 14.sp
                            )
                        },
                        singleLine = true,
                        maxLines = 1,
                        colors = TextFieldDefaults.colors(
                            unfocusedIndicatorColor = Color.Transparent,
                            focusedIndicatorColor = Color.Transparent,
                            focusedContainerColor = Color.Transparent,
                            unfocusedContainerColor = Color.Transparent,
                            focusedLabelColor = Color.Black,
                            unfocusedLabelColor = Color.Black,
                            focusedTextColor = MaterialTheme.colorScheme.onPrimary,
                            unfocusedTextColor = MaterialTheme.colorScheme.onPrimary,
                        ),
                        shape = RoundedCornerShape(5.dp)
                    )
                }
                Spacer(modifier = Modifier.height(5.dp))

                Row(Modifier.padding(start = 5.dp, end = 5.dp)) {
                    TextField(
                        value = lastname,
                        onValueChange = { newtext -> lastname = newtext },
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(60.dp)
                            .border(
                                width = 1.dp,
                                color = Color(0xFF312E2E),
                                shape = RoundedCornerShape(5.dp)
                            ),
                        label = {
                            Text(
                                text = "Enter your lastName",
                                color = Color(0xFF878787),
                                fontSize = 14.sp
                            )
                        },
                        singleLine = true,
                        maxLines = 1,
                        colors = TextFieldDefaults.colors(
                            unfocusedIndicatorColor = Color.Transparent,
                            focusedIndicatorColor = Color.Transparent,
                            focusedContainerColor = Color.Transparent,
                            unfocusedContainerColor = Color.Transparent,
                            focusedLabelColor = Color.Black,
                            unfocusedLabelColor = Color.Black,
                            focusedTextColor = MaterialTheme.colorScheme.onPrimary,
                            unfocusedTextColor = MaterialTheme.colorScheme.onPrimary,
                        ),
                        shape = RoundedCornerShape(5.dp)
                    )
                }
                Spacer(modifier = Modifier.height(16.dp))
                Row(Modifier.fillMaxWidth()) {
                    Button(
                        {
                            if (name.length >= 1 && isButtonEnabled && lastname.length >= 1) {
                                onNext()
                                Toast.makeText(ctx, "success", Toast.LENGTH_SHORT).show()
                            }
                            else if (name.length < 1 && lastname.length < 1) {
                                Toast.makeText(
                                    a,
                                    "Ensure that you use valid information ",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        },

                        modifier = Modifier
                            .fillMaxWidth()
                            .height(60.dp),
                        //.clip(RoundedCornerShape(5.dp)),
                        colors = ButtonDefaults.buttonColors(
                            containerColor = if (isButtonEnabled) Color(0xFF0070FA) else Color(
                                0xFFB8B8B8
                            )
                        )
                    ) {
                            Row(
                                modifier = Modifier.fillMaxWidth(),
                                horizontalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    text = "Continue",
                                    fontSize = 16.sp,
                                    color = if (isButtonEnabled) Color.White else Color.Gray // Change text color based on state
                                )
                            }
                    }
                }
            }

            3 -> {
                val a = LocalContext.current

                val isButtonEnabled = password.isNotEmpty() && againpass.isNotEmpty()
                Row(Modifier.padding(start = 5.dp, end = 5.dp)) {
                    TextField(
                        value = password,
                        onValueChange = { newtext -> password = newtext },
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(60.dp)
                            .border(
                                width = 1.dp,
                                color = Color(0xFF312E2E),
                                shape = RoundedCornerShape(5.dp)
                            ),
                        label = {
                            Text(
                                text = "Enter your password",
                                color = Color(0xFF878787),
                                fontSize = 14.sp
                            )
                        },
                        visualTransformation = if (passvisible) VisualTransformation.None else PasswordVisualTransformation(),
                        trailingIcon = {
                            val image1 = if (passvisible) {
                                painterResource(id = R.drawable.hidden)
                            } else {
                                painterResource(id = R.drawable.hidden)
                            }
                            IconButton(onClick = {
                                passvisible = !passvisible
                            }) {
                                Icon(
                                    painter = image1,
                                    contentDescription = if (passvisible) "Hide password" else "Show password",
                                    modifier = Modifier.size(20.dp),
                                    tint = MaterialTheme.colorScheme.onPrimary
                                )
                            }
                        },
                        singleLine = true,
                        maxLines = 1,
                        colors = TextFieldDefaults.colors(
                            unfocusedIndicatorColor = Color.Transparent,
                            focusedIndicatorColor = Color.Transparent,
                            focusedContainerColor = Color.Transparent,
                            unfocusedContainerColor = Color.Transparent,
                            focusedLabelColor = Color.Black,
                            unfocusedLabelColor = Color.Black,
                            focusedTextColor = MaterialTheme.colorScheme.onPrimary,
                            unfocusedTextColor = MaterialTheme.colorScheme.onPrimary,
                        ),
                        shape = RoundedCornerShape(5.dp)
                    )
                }
                Spacer(modifier = Modifier.height(5.dp))

                Row(Modifier.padding(start = 5.dp, end = 5.dp)) {
                    TextField(
                        value = againpass,
                        onValueChange = { newtext -> againpass = newtext },
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(60.dp)
                            .border(
                                width = 1.dp,
                                color = Color(0xFF312E2E),
                                shape = RoundedCornerShape(5.dp)
                            ),
                        label = {
                            Text(
                                text = "Enter your password again",
                                color = Color(0xFF878787),
                                fontSize = 14.sp
                            )
                        },
                        visualTransformation = if (againpassvisible) VisualTransformation.None else PasswordVisualTransformation(),
                        trailingIcon = {
                            val image2 = if (againpassvisible) {
                                painterResource(id = R.drawable.hidden)
                            } else {
                                painterResource(id = R.drawable.hidden)
                            }
                            IconButton(onClick = {
                                againpassvisible = !againpassvisible
                            }) {
                                Icon(
                                    painter = image2,
                                    contentDescription = if (againpassvisible) "Hide password" else "Show password",
                                    modifier = Modifier.size(20.dp),
                                    tint = MaterialTheme.colorScheme.onPrimary
                                )
                            }
                                       },
                        singleLine = true,
                        maxLines = 1,
                        colors = TextFieldDefaults.colors(
                            unfocusedIndicatorColor = Color.Transparent,
                            focusedIndicatorColor = Color.Transparent,
                            focusedContainerColor = Color.Transparent,
                            unfocusedContainerColor = Color.Transparent,
                            focusedLabelColor = Color.Black,
                            unfocusedLabelColor = Color.Black,
                            focusedTextColor = MaterialTheme.colorScheme.onPrimary,
                            unfocusedTextColor = MaterialTheme.colorScheme.onPrimary,
                        ),
                        shape = RoundedCornerShape(5.dp)
                    )
                }
                Spacer(modifier = Modifier.height(16.dp))
                Row(Modifier.fillMaxWidth()) {
                    Button(
                        onClick = {
                            if (
                                password.length >= 6 &&
                                password.any { it.isLetter() } &&
                                password == againpass &&
                                isButtonEnabled
                            ) {
                                RegisterViewModel.register(
                                    registerReq(
                                        checkemail,
                                        name,
                                        lastname,
                                        password,
                                        username
                                    )
                                )
                            } else {
                                if (password.length < 6) {
                                    Toast.makeText(
                                        a,
                                        "Password must be at least 6 characters",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                                if (!password.any { it.isLetter() }) {
                                    Toast.makeText(
                                        a,
                                        "Password must contain at least one letter",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                                if (password != againpass) {
                                    Toast.makeText(
                                        a,
                                        "Passwords do not match",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(60.dp),
                        //.clip(RoundedCornerShape(5.dp)),
                        colors = ButtonDefaults.buttonColors(
                            containerColor = if (isButtonEnabled) Color(0xFF0070FA) else Color(
                                0xFFB8B8B8
                            )
                        )
                    ) {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.Center
                        ) {
                            Text(
                                text = "Continue",
                                fontSize = 16.sp,
                                color = if (isButtonEnabled) Color.White else Color.Gray
                            )
                        }
                    }
                }
            }
        }

        Spacer(modifier = Modifier.height(16.dp))

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ) {
            if (currentState > 0) {
                Button(
                    onClick = onPrevious,
                    colors = ButtonDefaults.buttonColors(Color.Transparent)
                ) {
                    Text("go back!", color = MaterialTheme.colorScheme.onPrimary)
                }
            }
        }
    }
}
