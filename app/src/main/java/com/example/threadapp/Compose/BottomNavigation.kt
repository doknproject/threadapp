package com.example.threadapp.Compose

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.threadapp.R

@Composable
fun bottomNavigation(
    navController: NavHostController,
    chooseitem: MutableState<Int>,
    modifier: Modifier = Modifier
) {
    BottomAppBar(
        modifier = modifier
            .systemBarsPadding()
            .fillMaxWidth()
            .height(89.dp)
    ) {
        Row(
            modifier
                .fillMaxSize(),
            horizontalArrangement = Arrangement.SpaceEvenly,
            verticalAlignment = Alignment.CenterVertically
        ) {
            IconButton(onClick = {
                chooseitem.value = 1
                navController.navigate("HomeScreen")
            }) {
                Icon(
                    painter = painterResource(id = R.drawable.home),
                    contentDescription = null,
                    tint = if (chooseitem.value == 1) Color.Black else Color.White
                )
            }
            // 2
            IconButton(onClick = {
                chooseitem.value = 2
                navController.navigate("SearchScreen")
            }) {
                Icon(
                    painter = painterResource(id = R.drawable.zarebin),
                    contentDescription = null,
                    tint = if (chooseitem.value == 2) Color.Black else Color.White
                )
            }
            // 3
            IconButton(onClick = {
                chooseitem.value = 3
                navController.navigate("ThreadScreen")

            }) {
                Icon(
                    painter = painterResource(id = R.drawable.write),
                    contentDescription = null,
                    tint = if (chooseitem.value == 3) Color.Black else Color.White
                )
            }
            // 4
            IconButton(onClick = {
                chooseitem.value = 4
                navController.navigate("LikeScreen")
            }) {
                Icon(
                    painter = painterResource(id = R.drawable.heart),
                    contentDescription = null,
                    tint = if (chooseitem.value == 4) Color.Black else Color.White
                )
            }
            // 5
            IconButton(onClick = {
                chooseitem.value = 5
                navController.navigate("ProfileScreen")

            }) {
                Icon(
                    painter = painterResource(id = R.drawable.users),
                    contentDescription = null,
                    tint = if (chooseitem.value == 5) Color.Black else Color.White
                )
            }

        }
    }
}