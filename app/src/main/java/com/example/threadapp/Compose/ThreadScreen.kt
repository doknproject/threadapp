package com.example.threadapp.Compose

import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.threadapp.LogInData.SignInReq
import com.example.threadapp.MakeData.MakeReq
import com.example.threadapp.MakeData.MakeRes
import com.example.threadapp.R
import com.example.threadapp.viewmodel.MakeThreadViewModel

import java.time.LocalDateTime

//  viewModel.makethread(MakeReq(currentDateTime.toString(),makethread))

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun ThreadScreen(navController: NavController,viewModel: MakeThreadViewModel) {

    val profileRes by viewModel.makethread.observeAsState()
    val currentDateTime = LocalDateTime.now()

    var makethread by remember {
        mutableStateOf("")
    }

    val token = getToken(LocalContext.current) as String





    Column(
        Modifier
            .systemBarsPadding()
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(start = 10.dp, end = 10.dp, top = 25.dp),

        ) {

        Row(
            Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {

            IconButton(onClick = { navController.navigate("HomeScreen") }) {
                Icon(
                    modifier = Modifier.size(18.dp),
                    painter = painterResource(id = R.drawable.close),
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.onPrimary
                )
            }

            Text(
                text = "New Thread",
                fontSize = 18.sp,
                color = MaterialTheme.colorScheme.onPrimary
            )
        }

        Spacer(modifier = Modifier.height(20.dp))

        Row(Modifier.fillMaxWidth()) {
            Row(
                Modifier.fillMaxWidth(),
            ) {

                Box(
                    Modifier
                        .size(46.dp)
                        .clip(shape = RoundedCornerShape(22.dp))
                ) {
                    androidx.compose.foundation.Image(
                        painter = painterResource(
                            id = R.drawable.unknownpic
                        ),
                        contentDescription = null,
                        contentScale = ContentScale.Crop,

                        )
                }

                Spacer(modifier = Modifier.width(10.dp))

                Column {
                    Box(Modifier.fillMaxWidth()) {
                        Box() {
                            Text(
                                text = "Thread Here",
                                fontSize = 15.sp,
                                color = MaterialTheme.colorScheme.onPrimary
                            )
                        }
                        Box(
                            Modifier
                                .fillMaxWidth()
                                .offset(x = -15.dp, y = 15.dp)) {
                            TextField(
                                value = makethread ,
                                onValueChange = { newtext -> makethread = newtext },
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .height(60.dp),
                                label = {
                                    Text(
                                        text = "Start a Thread...",
                                        color = Color(0xFF878787),
                                        fontSize = 14.sp
                                    )
                                },
                                colors = TextFieldDefaults.colors(
                                    unfocusedIndicatorColor = Color.Transparent,
                                    focusedIndicatorColor = Color.Transparent,
                                    focusedContainerColor = Color.Transparent,
                                    unfocusedContainerColor = Color.Transparent,
                                    focusedLabelColor = Color.Transparent,
                                    unfocusedLabelColor = Color.Transparent,
                                    focusedTextColor = MaterialTheme.colorScheme.onPrimary,
                                    unfocusedTextColor = MaterialTheme.colorScheme.onPrimary,
                                    cursorColor = Color.Transparent,
                                ),
                              /*  keyboardActions = KeyboardActions(
                                    onDone = {
                                        // Trigger the ViewModel request on enter key press
                                        if (makethread.isNotEmpty()) {
                                            viewModel.makethread(
                                               token, MakeReq(currentDateTime.toString(), makethread)
                                            )
                                        }
                                    }
                                ),
                                keyboardOptions = KeyboardOptions.Default.copy(
                                    imeAction = ImeAction.Done
                                )*/
                            )
                        }

                    }

                    Row(
                        Modifier
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.End
                    ) {
                        val a = LocalContext.current

                        IconButton(onClick = {
                            if (makethread.isNotEmpty()) {
                                viewModel.makethread(
                                    token,MakeReq(currentDateTime.toString(), makethread)
                                )

                                // Clear the TextField after submission
                                makethread = ""
                            } else {
                                Toast.makeText(a, "Please enter text", Toast.LENGTH_SHORT).show()
                            }
                        }
                        ) {
                            Icon(
                                modifier = Modifier.size(24.dp),
                                painter = painterResource(id = R.drawable.send_svgrepo_com),
                                contentDescription = null,
                                tint = Color(0xFF858282)
                            )
                        }

                    }



                }



            }
        }
        }
}