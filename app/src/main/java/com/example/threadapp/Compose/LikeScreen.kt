package com.example.threadapp.Compose

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.scrollable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.threadapp.Function.Activity
import com.example.threadapp.R


@Composable
fun LikeScreen(navController: NavController) {
    var turn by remember { mutableStateOf(0) }
    val scrollState = rememberScrollState()

    val cardsData: List<Map<String, Any>> = listOf(


        mapOf("image" to R.drawable.womanpro,"name" to "Elina",),
        mapOf("image" to R.drawable.manprofile,"name" to "Rmando"),
        mapOf("image" to R.drawable.womanpro,"name" to "Sara"),
        mapOf("image" to R.drawable.manprofile,"name" to "Frank"),
        mapOf("image" to R.drawable.womanpro,"name" to "Bane"),
        mapOf("image" to R.drawable.manprofile,"name" to "Harly"),
        mapOf("image" to R.drawable.womanpro,"name" to "Olivia"),
        mapOf("image" to R.drawable.manprofile,"name" to "Michael"),
        mapOf("image" to R.drawable.womanpro,"name" to "Mina"),
        mapOf("image" to R.drawable.manprofile,"name" to "Amir"),

    )


    Column(
        Modifier
            .systemBarsPadding()
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
            .padding(start = 10.dp, end = 10.dp, top = 25.dp)
    ) {
        Text(
            text = "Activity",
            fontSize = 32.sp,
            color = MaterialTheme.colorScheme.onPrimary
        )

        Spacer(modifier = Modifier.height(10.dp))

        Row(
            Modifier
                .fillMaxWidth()
                .horizontalScroll(scrollState)
        ) {
            for ((index, category) in getAllCategory().withIndex()) {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier
                        .clip(shape = RoundedCornerShape(12.dp))
                        .clickable { turn = index }
                        .background(if (turn == index) MaterialTheme.colorScheme.onPrimary else Color.Transparent)
                        .border(1.dp, Color(0xFF2F2F2F), RoundedCornerShape(12.dp))
                        .width(112.dp)
                        .height(38.dp)
                        .padding(start = 11.dp, end = 11.dp)
                ) {
                    Text(
                        text = category.value,
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 14.sp,
                        color = if (turn == index) MaterialTheme.colorScheme.onSecondary else MaterialTheme.colorScheme.onPrimary
                    )
                }
                Spacer(Modifier.width(6.dp))
            }
        }

        Spacer(modifier = Modifier.height(20.dp))

        // i Used LazyColumn to display activities dynamically
        LazyColumn(
            modifier = Modifier.fillMaxSize()
        ) {
            items(cardsData) { data ->
                Activity(map = data)
            }
        }
    }
}

fun getAllCategory(): List<Category> {
    return listOf(
        Category("All"),
        Category("Follow"),
        Category("Like"),
        Category("Mention"),
        Category("Replies"),
    )
}

data class Category(val value: String)