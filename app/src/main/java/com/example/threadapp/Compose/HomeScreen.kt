package com.example.threadapp.Compose

import android.util.Log
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import com.example.threadapp.MakeListData.Data
import com.example.threadapp.MakeListData.User
import com.example.threadapp.R
import com.example.threadapp.viewmodel.GetListViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlin.math.log

@Composable
fun HomeScreen(viewModel: GetListViewModel, navController: NavController) {
    val thread by viewModel.getlist.observeAsState()
    val context = LocalContext.current
    val token = getToken(LocalContext.current) as String

    val loading = viewModel.loading.value

    for (i in 1..1){
        viewModel.fetchDataList(token)
    }
    var clicked by remember {
        mutableStateOf(false)
    }

    var isLoading by remember {
        mutableStateOf(true)
    }
    LaunchedEffect(key1 = true) {
        delay(1500)
        isLoading = false
    }

    /*
    LaunchedEffect(token) {
        if (token != null) {

        }
    }*/

    Column (
        modifier = Modifier
            .wrapContentSize()
            .clip(RoundedCornerShape(4.dp))
            .padding(start = 16.dp, end = 16.dp, top=16.dp, bottom = 100.dp)
    ) {
        if (thread?.data.isNullOrEmpty()) {
            Text("Loading")
        } else {
            val myData = thread?.data as List<Data>
            myData.let {
                LazyColumn {
                    items(it) { data ->
                        ShimmerListItem(
                            isLoading = isLoading,
                            contentAfterLoading = {
                                Column(Modifier.fillMaxSize()) {
                                    Row(
                                        Modifier.fillMaxWidth(),
                                        horizontalArrangement = Arrangement.SpaceBetween
                                    ) {

                                        Row() {
                                            Row(verticalAlignment = Alignment.Bottom) {
                                                Box(
                                                    Modifier
                                                        .size(46.dp)
                                                        .clip(shape = RoundedCornerShape(22.dp))
                                                ) {
                                                    androidx.compose.foundation.Image(
                                                        painter = painterResource(
                                                            id = R.drawable.womanpro
                                                        ),
                                                        contentDescription = null,
                                                        contentScale = ContentScale.Crop,

                                                        )
                                                }
                                                Box(modifier = Modifier.offset(x=-10.dp)) {
                                                    Box(
                                                        Modifier
                                                            .size(20.dp)
                                                            .clip(shape = RoundedCornerShape(10.dp))
                                                            .background(MaterialTheme.colorScheme.onPrimary)
                                                            .border(
                                                                1.dp, MaterialTheme.colorScheme.onSecondary,
                                                                RoundedCornerShape(10.dp)
                                                            )
                                                            .clickable { }

                                                    ){
                                                        IconButton(onClick = { }) {
                                                            Icon(
                                                                painter = painterResource(id = R.drawable.plussonpro),
                                                                contentDescription = null,
                                                                tint = MaterialTheme.colorScheme.onSecondary,
                                                                modifier = Modifier .size(10.dp)
                                                            )

                                                        }


                                                    }
                                                }

                                            }

                                            Column {
                                                Text(
                                                    text = data.user.firstName + " " + data.user.lastName,
                                                    fontSize = 16.sp,
                                                    color = MaterialTheme.colorScheme.onPrimary,
                                                    fontWeight = FontWeight.SemiBold
                                                )

                                                Spacer(modifier = Modifier.height(6.dp))

                                                Text(
                                                    text = data.text,
                                                    fontSize = 15.sp,
                                                    color = MaterialTheme.colorScheme.onPrimary,
                                                    fontWeight = FontWeight.Thin
                                                )

                                                Spacer(modifier = Modifier.height(10.dp))

                                                Row(verticalAlignment = Alignment.CenterVertically) {

                                                    Icon(
                                                        painter = painterResource(id = if (clicked) R.drawable.liked else R.drawable.unliked),
                                                        contentDescription = null,
                                                        tint = if(clicked)Color.Red  else MaterialTheme.colorScheme.onPrimary,
                                                        modifier = Modifier
                                                            .size(24.dp)
                                                            .clickable { clicked = !clicked }
                                                    )
                                                    Spacer(modifier = Modifier.width(12.dp))
                                                    Icon(
                                                        painter = painterResource(id = R.drawable.message2),
                                                        contentDescription = null,
                                                        tint = MaterialTheme.colorScheme.onPrimary,
                                                        modifier = Modifier
                                                            .size(20.dp)
                                                            .clickable { }
                                                    )
                                                    Spacer(modifier = Modifier.width(12.dp))
                                                    Icon(
                                                        painter = painterResource(id = R.drawable.message3),
                                                        contentDescription = null,
                                                        tint = MaterialTheme.colorScheme.onPrimary,
                                                        modifier = Modifier
                                                            .size(24.dp)
                                                            .clickable { }
                                                    )
                                                    Spacer(modifier = Modifier.width(12.dp))
                                                    Icon(
                                                        painter = painterResource(id = R.drawable.message4),
                                                        contentDescription = null,
                                                        tint = MaterialTheme.colorScheme.onPrimary,
                                                        modifier = Modifier
                                                            .size(26.dp)
                                                            .clickable { }
                                                    )
                                                }

                                                Spacer(modifier = Modifier.height(10.dp))

                                                Text(text = if(clicked)"1 Like" else "0 Like", fontSize = 16.sp, color = Color(0xFFA0A0A0))

                                            }
                                        }


                                        Row(verticalAlignment = Alignment.CenterVertically) {
                                            Text(text = "49m", fontSize = 12.sp, color = Color(0xFFA0A0A0))

                                            Spacer(modifier = Modifier.width(12.dp))

                                            Icon(
                                                painter = painterResource(id = R.drawable.three_dots_svgrepo_com),
                                                contentDescription = null,
                                                tint = MaterialTheme.colorScheme.onPrimary,
                                                modifier = Modifier
                                                    .size(24.dp)
                                                    .clickable { }
                                            )



                                        }
                                    }

                                    Spacer(modifier = Modifier.height(10.dp))

                                    Divider(
                                        modifier = Modifier
                                            .fillMaxWidth(),
                                        color = Color(0xFF3F3F3F)
                                    )
                                    Spacer(modifier = Modifier.height(5.dp))

                                }
                            }
                        )



                    }
                }
            }
        }
    }
}




/*
@Composable
fun HomeScreen(navController: NavHostController, viewModel: GetListViewModel) {

    val token = getToken(LocalContext.current) as String
    val getListState by viewModel.getlist.observeAsState()
    val s = "A_k"

    LaunchedEffect(token) {
        if (token != null) {
            Log.d(s, "1" + token!!)
            viewModel.getDataList(token)
        }
    }
    val user = getListState?.

    val size = getListState?.body()?.data?.size
    Log.d(s, "11111111" + size)


    // Show loading indicator if data is being loaded
    if (loading) {
        CircularProgressIndicator(modifier = Modifier.padding(16.dp))
    } else {
        // Handle the response state
        getListState?.let { response ->
            Log.d(s, "HomeScreen: Response: $response")
            if (response.isSuccessful) {
                val userList = response.body()?.data ?: emptyList()
                LazyColumn(modifier = Modifier.padding(16.dp)) {
                    items(userList) { data ->
                        UserItem(user = data.user)
                        Log.d(s, "User: ${data.user}")
                    }
                }
            } else {
                // Handle error state
                Text(text = "Failed to load data")
            }
        } ?: run {
            // Handle null state if required
            Text(text = "No data available")
        }
    }

}
*/



/*

            response.body()?.let { data ->
                if(response.isSuccessful){
                    LazyColumn(modifier = Modifier.systemBarsPadding().padding(bottom = 50.dp, start = 10.dp, end = 10.dp)) {
                        items(data.data) { item ->
                            val username = item.user.username
                            val text = item.text
                            val createdAt = item.createdAt


                            Column(Modifier.fillMaxSize()) {
                                Row(
                                    Modifier.fillMaxWidth(),
                                    horizontalArrangement = Arrangement.SpaceBetween
                                ) {

                                    Row() {
                                        Row(verticalAlignment = Alignment.Bottom) {
                                            Box(
                                                Modifier
                                                    .size(46.dp)
                                                    .clip(shape = RoundedCornerShape(22.dp))
                                            ) {
                                                androidx.compose.foundation.Image(
                                                    painter = painterResource(
                                                        id = R.drawable.unknownpic
                                                    ),
                                                    contentDescription = null,
                                                    contentScale = ContentScale.Crop,

                                                    )
                                            }
                                            Box(modifier = Modifier.offset(x=-10.dp)) {
                                                Box(
                                                    Modifier
                                                        .size(20.dp)
                                                        .clip(shape = RoundedCornerShape(10.dp))
                                                        .background(MaterialTheme.colorScheme.onPrimary)
                                                        .border(
                                                            1.dp, MaterialTheme.colorScheme.onSecondary,
                                                            RoundedCornerShape(10.dp)
                                                        )
                                                        .clickable { }

                                                ){
                                                    IconButton(onClick = { }) {
                                                        Icon(
                                                            painter = painterResource(id = R.drawable.plussonpro),
                                                            contentDescription = null,
                                                            tint = MaterialTheme.colorScheme.onSecondary,
                                                            modifier = Modifier .size(10.dp)
                                                        )

                                                    }


                                                }
                                            }

                                        }

                                        Column {
                                            Text(
                                                text = username,
                                                fontSize = 16.sp,
                                                color = MaterialTheme.colorScheme.onPrimary,
                                                fontWeight = FontWeight.SemiBold
                                            )

                                            Spacer(modifier = Modifier.height(6.dp))

                                            Text(
                                                text = text,
                                                fontSize = 15.sp,
                                                color = MaterialTheme.colorScheme.onPrimary,
                                                fontWeight = FontWeight.Thin
                                            )

                                            Spacer(modifier = Modifier.height(10.dp))

                                            Row(verticalAlignment = Alignment.CenterVertically) {

                                                Icon(
                                                    painter = painterResource(id =  R.drawable.liked ),
                                                    contentDescription = null,
                                                    modifier = Modifier
                                                        .size(24.dp)
                                                )
                                                Spacer(modifier = Modifier.width(12.dp))
                                                Icon(
                                                    painter = painterResource(id = R.drawable.message2),
                                                    contentDescription = null,
                                                    tint = MaterialTheme.colorScheme.onPrimary,
                                                    modifier = Modifier
                                                        .size(20.dp)
                                                        .clickable { }
                                                )
                                                Spacer(modifier = Modifier.width(12.dp))
                                                Icon(
                                                    painter = painterResource(id = R.drawable.message3),
                                                    contentDescription = null,
                                                    tint = MaterialTheme.colorScheme.onPrimary,
                                                    modifier = Modifier
                                                        .size(24.dp)
                                                        .clickable { }
                                                )
                                                Spacer(modifier = Modifier.width(12.dp))
                                                Icon(
                                                    painter = painterResource(id = R.drawable.message4),
                                                    contentDescription = null,
                                                    tint = MaterialTheme.colorScheme.onPrimary,
                                                    modifier = Modifier
                                                        .size(26.dp)
                                                        .clickable { }
                                                )
                                            }

                                            Spacer(modifier = Modifier.height(10.dp))

                                            Text(text ="1 Like", fontSize = 16.sp, color = Color(0xFFA0A0A0))

                                        }
                                    }


                                    Row(verticalAlignment = Alignment.CenterVertically) {
                                        Text(text = createdAt, fontSize = 12.sp, color = Color(0xFFA0A0A0))

                                        Spacer(modifier = Modifier.width(12.dp))

                                        Icon(
                                            painter = painterResource(id = R.drawable.three_dots_svgrepo_com),
                                            contentDescription = null,
                                            tint = MaterialTheme.colorScheme.onPrimary,
                                            modifier = Modifier
                                                .size(24.dp)
                                                .clickable { }
                                        )



                                    }
                                }

                                Spacer(modifier = Modifier.height(10.dp))

                                Divider(
                                    modifier = Modifier
                                        .fillMaxWidth(),
                                    color = Color(0xFF3F3F3F)
                                )

                            }
                        }
                    }

                }
                else{
                    Text("not available data")

                }

            }
        }
    }
}


*/



@Composable
fun UserItem(user: User) {
    Column(modifier = Modifier.padding(vertical = 8.dp)) {
        Text(text = "First Name: ${user.firstName}")
        Text(text = "Last Name: ${user.lastName}")
        Text(text = "Username: ${user.username}")
    }
}