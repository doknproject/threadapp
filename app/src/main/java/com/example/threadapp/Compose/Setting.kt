package com.example.threadapp.Compose

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.threadapp.R


@Composable
fun setting(navController: NavController) {

    Column(
        Modifier
            .systemBarsPadding()
            .fillMaxSize()
            .padding(start = 10.dp, end = 10.dp, top = 10.dp)
            .background(MaterialTheme.colorScheme.background)

    ) {

        Row(
            Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            IconButton(onClick = {navController.navigate("ProfileScreen")}) {
                Icon(
                    painter = painterResource(id = R.drawable.lefticon),
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.onPrimary,
                    modifier = Modifier .size(24.dp)
                )
            }
            Text(
                text = " Setting ",
                color = MaterialTheme.colorScheme.onPrimary,
                fontSize = 16.sp,
                modifier = Modifier.clickable {  }
            )
        }
        Spacer(modifier = Modifier.height(5.dp))
        Column(
            modifier = Modifier.padding(5.dp)
        ) {

            //2
            Row(
                Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    IconButton(onClick = { }) {
                        Icon(
                            painter = painterResource(id = R.drawable.oneuser),
                            contentDescription = null,
                            tint = MaterialTheme.colorScheme.onPrimary,
                            modifier = Modifier .size(40.dp)
                        )

                    }

                    Text(
                        text = " Follow and invite friends ",
                        color = MaterialTheme.colorScheme.onPrimary,
                        fontSize = 16.sp,
                        modifier = Modifier.clickable {  }
                    )

                }

            }
            Spacer(modifier = Modifier.height(5.dp))
            //3
            Row(
                Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton(onClick = { }) {
                    Icon(
                        painter = painterResource(id = R.drawable.notifsetting),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onPrimary,
                        modifier = Modifier .size(40.dp)
                    )

                }
                Text(
                    text = " Notifications",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontSize = 16.sp,
                    modifier = Modifier.clickable {  }
                )


            }
            //4
            Spacer(modifier = Modifier.height(5.dp))
            Row(
                Modifier.fillMaxWidth().clickable { navController.navigate("privacy") },
                verticalAlignment = Alignment.CenterVertically

            ) {
                IconButton(onClick = { }) {
                    Icon(
                        painter = painterResource(id = R.drawable.lock),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onPrimary,
                        modifier = Modifier .size(40.dp)
                    )

                }
                Text(
                    text = " Privacy ",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontSize = 16.sp,
                )


            }
            //5
            Spacer(modifier = Modifier.height(5.dp))
            Row(
                Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton(onClick = { }) {
                    Icon(
                        painter = painterResource(id = R.drawable._usercircle),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onPrimary,
                        modifier = Modifier .size(40.dp)
                    )

                }
                Text(
                    text = " Account ",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontSize = 16.sp,
                    modifier = Modifier.clickable {  }
                )

            }
            //6
            Spacer(modifier = Modifier.height(5.dp))
            Row(
                Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton( onClick = { } ) {
                    Icon(
                        painter = painterResource(id = R.drawable.languageicon),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onPrimary,
                        modifier = Modifier .size(40.dp)
                    )

                }
                Text(
                    text = " Language ",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontSize = 16.sp,
                    modifier = Modifier.clickable {  }
                )

            }
            //7
            Spacer(modifier = Modifier.height(5.dp))
            Row(
                Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton( onClick = { } ) {
                    Icon(
                        painter = painterResource(id = R.drawable.help),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onPrimary,
                        modifier = Modifier .size(40.dp)
                    )

                }
                Text(
                    text = " Help ",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontSize = 16.sp,
                    modifier = Modifier.clickable {  }
                )

            }
                //8
            Spacer(modifier = Modifier.height(5.dp))
            Row(
                Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton( onClick = { } ) {
                    Icon(
                        painter = painterResource(id = R.drawable.about),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onPrimary,
                        modifier = Modifier .size(40.dp)
                    )

                }
                Text(
                    text = "About",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontSize = 16.sp,
                    modifier = Modifier.clickable {  }
                )

            }

            Spacer(modifier = Modifier.height(10.dp))
            val a = LocalContext.current

            Row(Modifier.fillMaxWidth().clickable {
                navController.navigate("SignIn")
                clearToken(a)
            }) {

                Text(
                    text = " Log Out ",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontSize = 16.sp,
                    modifier = Modifier

                )


            }

            }

        }


    }


