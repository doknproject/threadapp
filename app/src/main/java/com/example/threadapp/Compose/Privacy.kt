package com.example.threadapp.Compose

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.threadapp.R
import com.example.threadapp.Function.switcher1

@Composable
fun privacy(navController: NavController) {

    Column(
        Modifier
            .systemBarsPadding()
            .fillMaxSize()
            .padding(start = 10.dp, end = 10.dp, top = 10.dp)
            .background(MaterialTheme.colorScheme.background)
    ) {

        Row(
            Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            IconButton(onClick = { navController.navigate("setting") }) {
                Icon(
                    painter = painterResource(id = R.drawable.lefticon),
                    contentDescription = null,
                    tint = MaterialTheme.colorScheme.onPrimary,
                    modifier = Modifier .size(24.dp)
                    )
            }
            Text(
                text = " Pivacy ",
                color = MaterialTheme.colorScheme.onPrimary,
                fontSize = 16.sp,
                modifier = Modifier.clickable {  }
            )
        }
        Spacer(modifier = Modifier.height(5.dp))
        Column(
            modifier = Modifier.padding(5.dp)
        ) {

            //2
            Row(
                Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    IconButton(onClick = { }) {
                        Icon(
                            painter = painterResource(id = R.drawable.lock),
                            contentDescription = null,
                            tint = MaterialTheme.colorScheme.onPrimary,
                            modifier = Modifier
                                .size(40.dp)
                                .clickable {  }
                        )

                    }

                    Text(
                        text = " Private profile ",
                        color = MaterialTheme.colorScheme.onPrimary,
                        fontSize = 16.sp,
                        modifier = Modifier.clickable {  }
                    )

                }

                switcher1()

            }
            Spacer(modifier = Modifier.height(5.dp))
            //3
            Row(
                Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton(onClick = { }) {
                    Icon(
                        painter = painterResource(id = R.drawable.thrednotif),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onPrimary,
                        modifier = Modifier .size(40.dp)
                    )

                }
                Text(
                    text = " Mentions",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontSize = 16.sp,
                    modifier = Modifier.clickable {  }
                )


            }
            //4
            Spacer(modifier = Modifier.height(5.dp))
            Row(
                Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton(onClick = { }) {
                    Icon(
                        painter = painterResource(id = R.drawable.muted),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onPrimary,
                        modifier = Modifier .size(40.dp)
                    )

                }
                Text(
                    text = " Muted ",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontSize = 16.sp,
                    modifier = Modifier.clickable {  }
                )


            }
            //5
            Spacer(modifier = Modifier.height(5.dp))
            Row(
                Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton(onClick = { }) {
                    Icon(
                        painter = painterResource(id = R.drawable.hidden),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onPrimary,
                        modifier = Modifier .size(40.dp)
                    )

                }
                Text(
                    text = " Hidden Words ",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontSize = 16.sp,
                    modifier = Modifier.clickable {  }
                )

            }
            //6
            Spacer(modifier = Modifier.height(5.dp))
            Row(
                Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                IconButton( onClick = { } ) {
                    Icon(
                        painter = painterResource(id = R.drawable.lefticon),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onPrimary,
                        modifier = Modifier .size(40.dp)
                    )

                }
                Text(
                    text = " Profiles you follow ",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontSize = 16.sp,
                    modifier = Modifier.clickable {  }
                )

            }

            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(start = 8.dp),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically

            )
            {
                Text(
                    text = "Other privacy setting",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontSize = 16.sp,
                    modifier = Modifier.clickable {  }
                )

                IconButton( onClick = { } ) {
                    Icon(
                        painter = painterResource(id = R.drawable.exitsetting),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onPrimary,
                        modifier = Modifier .size(32.dp)
                    )

                }

            }
            Row(
                modifier = Modifier
                    .padding(start = 10.dp)
                    .clickable {  }
            ) {
                Text(
                    text = "Some settings, like restricting, apply to both",
                    color =Color(0xFFB8B8B8),
                    fontSize = 16.sp,
                )

            }
            Row(
                modifier = Modifier
                    .padding(start = 10.dp)
                    .clickable {  }
            ) {
                Text(
                    text = "threads and instagram and can be managed",
                    color =Color(0xFFB8B8B8),
                    fontSize = 16.sp,
                )

            }
            Row(
                modifier = Modifier
                    .padding(start = 10.dp)
                    .clickable {  }
            ) {
                Text(
                    text = "on instagram",
                    color =Color(0xFFB8B8B8),
                    fontSize = 16.sp,
                )

            }
            Spacer(modifier = Modifier.height(5.dp))
            // out of long text
            Row(
                Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    IconButton(onClick = { }) {
                        Icon(
                            painter = painterResource(id = R.drawable.heartkhat),
                            contentDescription = null,
                            tint = MaterialTheme.colorScheme.onPrimary,
                            modifier = Modifier .size(40.dp)
                        )

                    }

                    Text(
                        text = " Hide likes ",
                        color = MaterialTheme.colorScheme.onPrimary,
                        fontSize = 16.sp,
                        modifier = Modifier.clickable {  }
                    )


                }

                IconButton( onClick = { } ) {
                    Icon(
                        painter = painterResource(id = R.drawable.exitsetting),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onPrimary,
                        modifier = Modifier .size(32.dp)
                    )


                }


            }


        }


    }

}
