package com.example.threadapp.Compose

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.threadapp.LogInData.SignInReq
import com.example.threadapp.R
import com.example.threadapp.ui.theme.gwendoly
import com.example.threadapp.viewmodel.LoginViewModel
import com.google.gson.Gson
import kotlin.system.exitProcess
import android.content.SharedPreferences
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import com.example.threadapp.Function.rememberImeState
import com.example.threadapp.SignUpData.checkUserNameReq

/*
Text(
text = "Price :",
color = MaterialTheme.colorScheme.onPrimary,
fontFamily = Sora,
fontSize = 14.sp,
fontWeight = FontWeight.Medium
)
*/

class responseError(
    val statusCode : Int,
    val message: String
){

}
/*fun getToken(context: Context): String? {
    val sharedPreferences: SharedPreferences = context.getSharedPreferences("user_prefs", Context.MODE_PRIVATE)
    return sharedPreferences.getString("access_token", null)
}*/

fun getToken(context: Context): String? {
    val sharedPreferences = context.getSharedPreferences("user_prefs", Context.MODE_PRIVATE)
    return sharedPreferences.getString("access_token", null)
}

fun saveToken(context: Context, token: String) {
    val sharedPreferences: SharedPreferences = context.getSharedPreferences("user_prefs", Context.MODE_PRIVATE)
    val editor = sharedPreferences.edit()
    editor.putString("access_token", token)
    editor.apply()
}

fun clearToken(context: Context) {
    val sharedPreferences: SharedPreferences = context.getSharedPreferences("user_prefs", Context.MODE_PRIVATE)
    val editor = sharedPreferences.edit()
    editor.remove("access_token")
    editor.apply()
}

@Composable
fun SignIn(navController: NavController, viewModel: LoginViewModel,/*viewModel2: GetListViewModel*/ ) {
    var username by remember { mutableStateOf("arman@gmail.com") }
    var password by remember { mutableStateOf("a123456") }
    val loginState by viewModel.login.observeAsState(null)
    var errorMessage by remember { mutableStateOf<String?>(null) }
    var passwordVisible by remember { mutableStateOf(false) }
    val loading = viewModel.loading.value
    val context = LocalContext.current

    /*var listViewmodel by viewModel2.getlist.observeAsState()*/

    LaunchedEffect(Unit) {
        clearToken(context) // Clear token before sign-in attempt
    }


    val imeState = rememberImeState()
    val scrollState = rememberScrollState()

    LaunchedEffect(key1 = imeState.value) {
        if (imeState.value){
            scrollState.animateScrollTo(scrollState.maxValue, tween(300))
        }
    }



    val token = getToken(LocalContext.current)

    /* BackHandler {
        // Handle the back button press here
        // You can exit the app or perform any other desired action
        navController.popBackStack()
    }*/


    BackHandler {
        // Exit the app
        exitProcess(0)
    }


    loginState?.let { loginRes ->
        if (loginRes.isSuccessful) {
            val signInResponse = loginRes.body()!!
            saveToken(context, signInResponse.access_token)

            val ctx = LocalContext.current
            Toast.makeText(ctx, "Succus", Toast.LENGTH_SHORT).show()
           // navController.navigate("ProfileScreen/$token")
            navController.navigate("HomeScreen")
            //ino bede be navigate
            /*/${loginRes.body()!!.user.email}*/
        } else {
            val ctx = LocalContext.current
            val gson = Gson()
            val errorResult = gson.fromJson(loginRes.errorBody()!!.string().toString(), responseError::class.java)
            Log.i("TAG", "p4: ${errorResult.toString()}")
            errorMessage = when (loginRes.code()) {
                in 500..599 -> "server Error!"

                in 400..499 -> "password is wrong!"
                else -> "please try again later."
            }
        }
    }


    val ctx = LocalContext.current

    LaunchedEffect(errorMessage) {
        if (errorMessage != null) {
            Toast.makeText(ctx, errorMessage, Toast.LENGTH_SHORT).show()
            errorMessage = null
        }
    }
    Column(Modifier.fillMaxSize().verticalScroll(scrollState).background(MaterialTheme.colorScheme.background)) {

        Column(
            modifier = Modifier
                .systemBarsPadding()
                .fillMaxWidth()
                .fillMaxHeight(0.92f)
                .padding(start = 10.dp, end = 10.dp, top = 10.dp)
        ) {
            Spacer(modifier = Modifier.height(5.dp))

            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                IconButton(onClick = { }) {
                    Icon(
                        painter = painterResource(id = R.drawable.threads__app__logo),
                        contentDescription = null,
                        tint = MaterialTheme.colorScheme.onPrimary,
                        modifier = Modifier .size(72.dp)
                    )
                }
            }

            Spacer(modifier = Modifier.height(120.dp))

            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center,
            ) {
                Text(
                    text = "Threads",
                    color = MaterialTheme.colorScheme.onPrimary,
                    fontFamily = gwendoly,
                    fontSize = 50.sp,
                    fontWeight = FontWeight.Normal
                )
            }

            Spacer(modifier = Modifier.height(25.dp))

            Row(Modifier.padding(start = 5.dp, end = 5.dp)) {
                TextField(
                    value = username,
                    onValueChange = { newtext -> username = newtext },
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(60.dp)
                        .border(
                            width = 1.dp,
                            color = Color(0xFF312E2E),
                            shape = RoundedCornerShape(5.dp)
                        ),
                    label = {
                        Text(
                            text = "Enter your email",
                            color = Color(0xFF878787),
                            fontSize = 14.sp
                        )
                    },
                    singleLine = true,
                    maxLines = 1,
                    colors = TextFieldDefaults.colors(
                        unfocusedIndicatorColor = Color.Transparent,
                        focusedIndicatorColor = Color.Transparent,
                        focusedContainerColor = Color.Transparent,
                        unfocusedContainerColor = Color.Transparent,
                        focusedLabelColor = Color.Black,
                        unfocusedLabelColor = Color.Black,
                        focusedTextColor = MaterialTheme.colorScheme.onPrimary,
                        unfocusedTextColor = MaterialTheme.colorScheme.onPrimary,
                    ),
                    shape = RoundedCornerShape(5.dp)
                )
            }

            Spacer(modifier = Modifier.height(10.dp))

            Row(modifier = Modifier.padding(start = 5.dp, end = 5.dp)) {
                TextField(
                    value = password,
                    onValueChange = { newtext -> password = newtext },
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(60.dp)
                        .border(
                            width = 1.dp,
                            color = Color(0xFF312E2E),
                            shape = RoundedCornerShape(5.dp)
                        ),
                    label = {
                        Text(
                            text = "Password",
                            color = Color(0xFF878787),
                            fontSize = 14.sp
                        )
                    },
                    visualTransformation = if (passwordVisible) VisualTransformation.None else PasswordVisualTransformation(),
                    trailingIcon = {
                        val image = if (passwordVisible) {
                            painterResource(id = R.drawable.hidden)
                        } else {
                            painterResource(id = R.drawable.hidden)
                        }

                        IconButton(onClick = {
                            passwordVisible = !passwordVisible
                        }) {
                            Icon(
                                painter = image,
                                contentDescription = if (passwordVisible) "Hide password" else "Show password",
                                modifier = Modifier.size(20.dp),
                                tint = MaterialTheme.colorScheme.onPrimary
                            )
                        }
                    },
                    singleLine = true,
                    maxLines = 1,
                    colors = TextFieldDefaults.colors(
                        unfocusedIndicatorColor = Color.Transparent,
                        focusedIndicatorColor = Color.Transparent,
                        focusedContainerColor = Color.Transparent,
                        unfocusedContainerColor = Color.Transparent,
                        focusedLabelColor = Color.Black,
                        unfocusedLabelColor = Color.Black,
                        focusedTextColor = MaterialTheme.colorScheme.onPrimary,
                        unfocusedTextColor = MaterialTheme.colorScheme.onPrimary,
                        cursorColor = MaterialTheme.colorScheme.onPrimary
                    ),
                    shape = RoundedCornerShape(5.dp)
                )
            }

            Spacer(modifier = Modifier.height(10.dp))

            Row(Modifier.padding(start = 5.dp, end = 5.dp)) {
                val a = LocalContext.current
                val isButtonEnabled = username.isNotEmpty() && password.isNotEmpty()

                Button(
                    {
                        if (isButtonEnabled) {
                            viewModel.login(SignInReq(username, password))
                           /* if (token != null) {
                                viewModel2.getlist(token)
                            }*/

                        }
                        else{
                            Toast.makeText(
                                a,
                                "Ensure that you use valid information ",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(60.dp),
                    //.clip(RoundedCornerShape(5.dp)),
                    colors = ButtonDefaults.buttonColors(
                        containerColor = if (isButtonEnabled) Color(0xFF0070FA) else Color(0xFFB8B8B8)
                    )
                ) {
                    if (loading == true) {
                        CircularProgressIndicator(
                            color = Color(0xFFB8B8B8),
                            modifier = Modifier.size(24.dp)
                        )
                    } else {
                        Row(
                            modifier = Modifier.fillMaxWidth(),
                            horizontalArrangement = Arrangement.Center
                        ) {
                            Text(
                                text = "Log in",
                                fontSize = 16.sp,
                                color = if (isButtonEnabled) Color.White else Color.Gray
                            )
                        }
                    }
                }
            }
            Row(
                Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                errorMessage?.let { message ->
                    Spacer(modifier = Modifier.height(10.dp))
                    Text(
                        text = "$message!",
                        color = Color.Red,
                        modifier = Modifier.padding(10.dp),
                    )
                }
            }

            Spacer(modifier = Modifier.height(15.dp))

        }
        Spacer(modifier = Modifier.weight(1f))
        Divider(
            modifier = Modifier
                .fillMaxWidth(),
            color = Color(0xFF3F3F3F)
        )

        Row(
            Modifier
                .fillMaxSize()
                .clickable {
                    navController.navigate("SignUp")
                },
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically,

        ) {

            Text("Don't have any account?",
                color = Color(0xFF878787),
                fontSize = 12.sp
            )
             Text(text = "SignUp", color = Color.Cyan,fontSize = 12.sp)

        }

    }

}