package com.example.threadapp.service

import com.example.threadapp.LogInData.SignInReq
import com.example.threadapp.LogInData.SignInRes
import com.example.threadapp.MakeData.MakeReq
import com.example.threadapp.MakeData.MakeRes
import com.example.threadapp.MakeListData.MakeListDataRes
import com.example.threadapp.SignUpData.checkEmailReq
import com.example.threadapp.SignUpData.checkEmailRes
import com.example.threadapp.SignUpData.checkUserNameReq
import com.example.threadapp.SignUpData.checkUserNameRes
import com.example.threadapp.SignUpData.registerReq
import com.example.threadapp.SignUpData.registerRes
import com.example.threadapp.ThreadData.ThreadDataReq
import com.example.threadapp.ThreadData.ThreadDataRes
import com.example.threadapp.profileData.ProfileData
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface LocalService {

    @POST("auth/login")
    suspend fun login(@Body body: SignInReq): Response<SignInRes>

    @POST("auth/check-email")
    suspend fun checkEmail(@Body body: checkEmailReq):Response<checkEmailRes>

    @POST("auth/check-username")
    suspend fun checkUserName(@Body body : checkUserNameReq):Response<checkUserNameRes>

    @POST("auth/register")
    suspend fun register(@Body body : registerReq): Response<registerRes>

    //ine
    @GET("post/list")
    suspend fun getDataList(@Header("Authorization") token: String): MakeListDataRes

    @POST("post/create")
    suspend fun createThread(@Body body : ThreadDataReq,@Header("Authorization") token: String) : Response<ThreadDataRes>

    @GET("user/profile")
    suspend fun  Profile(@Header("Authorization") token: String): Response<ProfileData>

    @POST("post/create")
    suspend fun Create(@Body body : MakeReq,@Header("Authorization") token: String) : Response<MakeRes>

}





/*
<!--<?xml version="1.0" encoding="utf-8"?>&lt;!&ndash;
Sample backup rules file; uncomment and customize as necessary.
See https://developer.android.com/guide/topics/data/autobackup
for details.
Note: This file is ignored for devices older that API 31
See https://developer.android.com/about/versions/12/backup-restore
&ndash;&gt;
<full-backup-content>
&lt;!&ndash;
<include domain="sharedpref" path="."/>
<exclude domain="sharedpref" path="device.xml"/>
&ndash;&gt;
</full-backup-conten*//*
t>-->*/
