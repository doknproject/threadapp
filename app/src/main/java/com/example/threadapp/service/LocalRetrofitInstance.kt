package com.example.threadapp.service

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object LocalRetrofitInstance {
    //private const val BASE_URL = "http://192.168.1.102:3000/api/"
    // private const val BASE_URL = "http://192.168.13.125:3000/api/"
   // private const val BASE_URL = "http://192.168.13.125:3000/api/"
   /* https://recursing-colden-ps6dvonyy.liara.run/api/auth/login*/
   // private const val BASE_URL = "http://192.168.1.38:3000/api/"
   // private const val BASE_URL = "http://192.168.20.144:3000/api/"
    private const val BASE_URL = "http://192.168.1.38:3000/api/"

    val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    val localService: LocalService by lazy {
        retrofit.create(LocalService::class.java)
    }
}