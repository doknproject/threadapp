package com.example.threadapp.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)

sealed class ThemeColors(
    val background: Color,
    val surface: Color,
    val primary: Color,
    val text: Color,
    val bottomController : Color,
    val bottomColor : Color,
    val onprimary : Color,
    val Secondary : Color,
    val OnSecondary : Color
) {
    object Night : ThemeColors(
        background =  Color(0xFFFFFFFF),
        surface =   Color(0xFF292E3F),// column asli home page va top bar
        primary =  Color(0xFF323748), //// Boxhaye Homepage
        text =   Color(0xFFFFFFFF), // onprimary text haye safhe
        bottomController  = Color(0xFFFFFFFF),
        bottomColor = Color(0xFF323748),
        onprimary = Color(0xFFFFFFFF),
        Secondary = Color(0xFF292E3F), // matn zire authenticator app
        OnSecondary = Color(0xFF000000)


    )

    object Day : ThemeColors(
        background =  Color(0xFF000000),
        surface =   Color(0xFFF4F6FC), //Column asli home page va top bar
        primary =   Color(0xFFF7F9FF), //Box haye HomePage
        text =   Color(0xFF292E3F),
        bottomController = Color(0xFF292E3F),
        bottomColor = Color(0xFFEFF1F7),
        onprimary = Color(0xFFFFFFFF),
        Secondary = Color(0xFFFFFFFF),
        OnSecondary = Color(0xFFFFFFFF)
    )
}
