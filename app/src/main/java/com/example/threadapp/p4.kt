/*
package com.example.threadapp

import android.util.Log
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonColors
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.threadapp.LogInData.SignInReq
import com.example.threadapp.viewmodel.LoginViewModel
import com.google.gson.Gson


class responseError(
    val statusCode : Int,
    val message: String
){

}
@Composable
fun p4(navController: NavController, viewModel: LoginViewModel) {
    var username by remember { mutableStateOf("") }
    var password by remember { mutableStateOf("") }
    val loginState by viewModel.login.observeAsState(null)
    var errorMessage by remember { mutableStateOf<String?>(null) }

    // Handle navigation based on login state
    loginState?.let { loginRes ->
        if (loginRes.isSuccessful) {
            val ctx = LocalContext.current
            Toast.makeText(ctx, "Succus", Toast.LENGTH_SHORT).show()
            navController.navigate("nextScreen/${loginRes.body()!!.user.email}")
        } else {
            val ctx = LocalContext.current
            val gson = Gson()
            val errorResult = gson.fromJson(loginRes.errorBody()!!.string().toString(), responseError::class.java)
            Log.i("TAG", "p4: ${errorResult.toString()}")
            errorMessage = when (loginRes.code()) {
                in 500..599 -> "server Error"
                in 400..499 -> "password is wrong"
                else -> "please try again later"
            }
        }
    }

    val ctx = LocalContext.current

    LaunchedEffect(errorMessage) {
        if (errorMessage != null) {
            Toast.makeText(ctx, errorMessage, Toast.LENGTH_SHORT).show()
            errorMessage = null
        }
    }
    Column(
        modifier = Modifier
            .systemBarsPadding()
            .fillMaxSize()
            .padding(start = 10.dp, end = 10.dp, top = 10.dp)
    ) {
        Spacer(modifier = Modifier.height(5.dp))

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ) {
            Box(
                modifier = Modifier.size(72.dp)
            ) {
                Image(
                    painter = painterResource(id = R.drawable.insta),
                    contentDescription = null,
                    modifier = Modifier.size(72.dp),
                    contentScale = ContentScale.Crop
                )
            }
        }

        Spacer(modifier = Modifier.height(150.dp))

        Row {
            TextField(
                value = username,
                onValueChange = { newtext -> username = newtext },
                modifier = Modifier
                    .fillMaxWidth()
                    .height(70.dp)
                    .border(width = 1.dp, color = Color.Black, shape = RoundedCornerShape(10.dp)),
                label = {
                    Text(
                        text = "Username, email or mobile number",
                        color = Color(0xFF878787)
                    )
                },
                singleLine = true,
                maxLines = 1,
                colors = TextFieldDefaults.colors(
                    unfocusedIndicatorColor = Color.Transparent,
                    focusedIndicatorColor = Color.Transparent,
                    focusedContainerColor = Color.Transparent,
                    unfocusedContainerColor = Color.Transparent,
                    focusedLabelColor = Color.Black,
                    unfocusedLabelColor = Color.Black,
                    focusedTextColor = Color.Black,
                    unfocusedTextColor = Color.Black
                ),
                shape = RoundedCornerShape(10.dp)
            )
        }

        Spacer(modifier = Modifier.height(10.dp))

        Row {
            TextField(
                value = password,
                onValueChange = { newtext -> password = newtext },
                modifier = Modifier
                    .fillMaxWidth()
                    .height(70.dp)
                    .border(
                        width = 1.dp,
                        color = Color(0xFF312E2E),
                        shape = RoundedCornerShape(10.dp)
                    ),
                label = {
                    Text(
                        text = "Password",
                        color = Color(0xFF878787)
                    )
                },
                singleLine = true,
                maxLines = 1,
                colors = TextFieldDefaults.colors(
                    unfocusedIndicatorColor = Color.Transparent,
                    focusedIndicatorColor = Color.Transparent,
                    focusedContainerColor = Color.Transparent,
                    unfocusedContainerColor = Color.Transparent,
                    focusedLabelColor = Color.Black,
                    unfocusedLabelColor = Color.Black,
                    focusedTextColor = Color.Black,
                    unfocusedTextColor = Color.Black
                ),
                shape = RoundedCornerShape(10.dp)
            )
        }

        Spacer(modifier = Modifier.height(10.dp))

        Row(Modifier.fillMaxWidth()) {
            Button(
                onClick = {
                    viewModel.login(SignInReq(username, password))
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .height(70.dp)
                    .clip(RoundedCornerShape(10.dp)),
                colors = ButtonDefaults.buttonColors(
                    containerColor = Color(0xFF0070FA)
                )
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center
                ) {
                    Text(text = "Log in", fontSize = 16.sp, color = Color.White)
                }
            }
        }

        errorMessage?.let { message ->
            Spacer(modifier = Modifier.height(10.dp))
            Text(
                text = message,
                color = Color.Red,
                modifier = Modifier.padding(10.dp),
            )
        }
    }
}


*/
/*
@Composable
fun p4(navController: NavController) {

    var username by remember {
        mutableStateOf("")
    }
    var password by remember {
        mutableStateOf("")
    }

    Column(
        Modifier
            .systemBarsPadding()
            .fillMaxSize()
            .padding(start = 10.dp, end = 10.dp, top = 10.dp)
    )
    {
        Spacer(modifier = Modifier.height(100.dp))

        Row(
            Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ) {

            Box(
                modifier = Modifier
                    .size(72.dp)

            ) {
                Image(
                    painter = painterResource(id = R.drawable.insta) ,
                     contentDescription = null,
                    Modifier
                        .size(72.dp),
                    contentScale = ContentScale.Crop

                )
            }

        }

        Spacer(modifier = Modifier.height(150.dp))

        Row() {
            TextField(   value = username, onValueChange = { newtext ->
                username = newtext
            },
                modifier = Modifier
                    .fillMaxWidth()
                    .height(70.dp)
                    .border(width = 1.dp, color = Color.Black, shape = RoundedCornerShape(10.dp)),
                label = {
                    Text(
                        text = "Username,email or mobile number",
                        color = Color(0xFF878787)
                    )
                },
                singleLine = true, maxLines = 1,
                colors = TextFieldDefaults.colors(
                    unfocusedIndicatorColor = Color.Transparent,
                    focusedIndicatorColor = Color.Transparent,
                    focusedContainerColor = Color.Transparent,
                    unfocusedContainerColor = Color.Transparent,
                    focusedLabelColor = Color.Black,
                    unfocusedLabelColor = Color.Black,
                    focusedTextColor = Color.Black,
                    unfocusedTextColor = Color.Black
                ),
                shape = RoundedCornerShape(10.dp)
            )

        }

        Spacer(modifier = Modifier.height(10.dp))

        Row() {
            TextField(   value = password, onValueChange = { newtext ->
                password = newtext
            },
                modifier = Modifier
                    .fillMaxWidth()
                    .height(70.dp)
                    .border(
                        width = 1.dp,
                        color = Color(0xFF312E2E),
                        shape = RoundedCornerShape(10.dp)
                    ),

                label = {
                    Text(
                        text = "Password",
                        color = Color(0xFF878787)
                    )
                },


                singleLine = true, maxLines = 1,
                colors = TextFieldDefaults.colors(
                    unfocusedIndicatorColor = Color.Transparent,
                    focusedIndicatorColor = Color.Transparent,
                    focusedContainerColor = Color.Transparent,
                    unfocusedContainerColor = Color.Transparent,
                    focusedLabelColor = Color.Black,
                    unfocusedLabelColor = Color.Black,
                    focusedTextColor = Color.Black,
                    unfocusedTextColor = Color.Black
                ),
                shape = RoundedCornerShape(10.dp)
            )

        }

        Spacer(modifier = Modifier.height(10.dp))



        Row(Modifier.fillMaxWidth()) {

            Button(
                onClick = { *//*

*/
/*TODO*//*
*/
/*
 },
                modifier = Modifier
                    .fillMaxWidth()
                    .height(70.dp)
                    .clip(RoundedCornerShape(10.dp)),
                colors = ButtonDefaults.buttonColors(
                    containerColor = Color(0xFF0070FA)
                )


            ) {
                Row (
                    Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center
                ){
                    Text(text = "Log in", fontSize = 16.sp, color = Color.White)

                }

            }

        }

    }

}*/
