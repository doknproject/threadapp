package com.example.threadapp.repository

import com.example.threadapp.LogInData.SignInReq
import com.example.threadapp.LogInData.SignInRes
import com.example.threadapp.MakeData.MakeReq
import com.example.threadapp.MakeData.MakeRes
import com.example.threadapp.MakeListData.MakeListDataRes
import com.example.threadapp.SignUpData.checkEmailReq
import com.example.threadapp.SignUpData.checkEmailRes
import com.example.threadapp.SignUpData.checkUserNameReq
import com.example.threadapp.SignUpData.checkUserNameRes
import com.example.threadapp.SignUpData.registerReq
import com.example.threadapp.SignUpData.registerRes
import com.example.threadapp.ThreadData.ThreadDataReq
import com.example.threadapp.ThreadData.ThreadDataRes
import com.example.threadapp.profileData.ProfileData
import com.example.threadapp.service.LocalRetrofitInstance
import retrofit2.Response

class LocalRepository {
    private val localService = LocalRetrofitInstance.localService

    suspend fun login(loginReq: SignInReq): Response<SignInRes> {
        return localService.login(loginReq);
    }

    suspend fun checkEmail(checkEmailReq: checkEmailReq): Response<checkEmailRes> {
        return localService.checkEmail(checkEmailReq);
    }

    suspend fun checkUserNameReq(checkUserNameReq:checkUserNameReq) : Response<checkUserNameRes>{
        return localService.checkUserName(checkUserNameReq);
    }

    suspend fun register(registerReq: registerReq) : Response<registerRes>{
        return localService.register(registerReq)
    }

    suspend fun makeThread(ThreadDataReq : ThreadDataReq,token: String) : Response<ThreadDataRes>{
        return localService.createThread(ThreadDataReq,"Bearer $token")
    }

    suspend fun getDataList(token: String): MakeListDataRes {
        return localService.getDataList("Bearer $token")
    }

    suspend fun profileData(token: String): Response<ProfileData>{
        return localService.Profile("Bearer $token")
    }

    suspend fun Create(makeReq: MakeReq,token:String): Response<MakeRes>{
        return localService.Create(makeReq,"Bearer $token")
    }

}