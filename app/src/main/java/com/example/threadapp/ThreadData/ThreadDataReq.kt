package com.example.threadapp.ThreadData

data class ThreadDataReq(
    val data : ThreadData,
    val user : ThreadUser,
    val message : String,
    val status : Int

)

data class ThreadData(
    val creatat : String,
    val id : String,
    val text : String
)

data class ThreadUser(
    val email : String,
    val iat : Int,
    val id : String
)

