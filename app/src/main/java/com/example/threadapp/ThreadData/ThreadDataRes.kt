package com.example.threadapp.ThreadData

data class ThreadDataRes(
    val `data`: Data,
    val message: String,
    val status: Int
)

data class User(
    val email: String,
    val iat: Int,
    val id: String
)

data class Data(
    val createdAt: String,
    val id: String,
    val text: String,
    val user: User
)