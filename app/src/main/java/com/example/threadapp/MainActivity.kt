package com.example.threadapp

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.threadapp.viewmodel.LoginViewModel
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import com.example.threadapp.Compose.Editprofile
import com.example.threadapp.Compose.HomeScreen
//import com.example.threadapp.Compose.ImageWithCaching
import com.example.threadapp.Compose.LikeScreen
import com.example.threadapp.Compose.PickImageFromGallery
import com.example.threadapp.Compose.ProfileScreen
import com.example.threadapp.Compose.SearchScreen
import com.example.threadapp.Compose.privacy
import com.example.threadapp.Compose.setting
import com.example.threadapp.Compose.SignIn
import com.example.threadapp.Compose.SignUp
import com.example.threadapp.Compose.ThreadScreen
import com.example.threadapp.Compose.bottomNavigation
import com.example.threadapp.ui.theme.MyApplicationTheme
import com.example.threadapp.viewmodel.GetListViewModel
import com.example.threadapp.viewmodel.MakeThreadViewModel

/*
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()
            Surface(color = MaterialTheme.colorScheme.background) {
                var currentState by remember { mutableStateOf(0) }

                register(
                    currentState = currentState,
                    onNext = {
                        currentState = (currentState + 1) % 4
                    },
                    onPrevious = {
                        currentState = if (currentState == 0) 3 else currentState - 1
                    }
                )
            }
        }
    }
}*/
/*class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ThreadAppTheme {
                val navController = rememberNavController()
                val showButtonBar = remember { mutableStateOf(true) }
                var currentState by remember { mutableStateOf(0) }
                val chooseItem = remember { mutableStateOf(1) }

                Scaffold(
                    modifier = Modifier.fillMaxSize(),
                    bottomBar = { bottomNavigation(navController, showButtonBar.value, chooseItem.value) }
                ) { innerPadding ->
                    NavHost(
                        navController = navController,
                        startDestination = "checkEmail",
                        modifier = Modifier.padding(innerPadding)
                    ) {
                   *//*     composable("checkEmail") {
                            CheckEmail(navController = navController, viewModel = CheckEmailViewModel())
                        }
                        composable("checkUserName") {
                            CheckUserName(navController = navController, viewModel = CheckUserNameViewModel())
                        }
                        composable("information") {
                            Information(navController = navController)
                        }
                        composable("SignUp") {
                            SignUp(navController = navController, FirstName = "", LastName = "")
                        }*//*
                        composable("register") {
                            register(
                                currentState = currentState,
                                onNext = {
                                    currentState = (currentState + 1) % 4
                                },
                                onPrevious = {
                                    currentState = if (currentState == 0) 3 else currentState - 1
                                },
                                navController = navController
                            )
                        }
                        composable("p1") {
                            showButtonBar.value = true
                            p1(navController = navController)
                            chooseItem.value = 1
                        }
                        composable("p2") {
                            showButtonBar.value = true
                            p2(navController = navController)
                            chooseItem.value = 2
                        }
                        composable("p3") {
                            showButtonBar.value = true
                            p3(navController = navController)
                            chooseItem.value = 3
                        }
                        composable("p4") {
                            showButtonBar.value = true
                            p4(navController = navController, viewModel = LoginViewModel())
                            chooseItem.value = 4
                        }
                        composable(
                            "nextScreen/{email}",
                            arguments = listOf(navArgument("email") { type = NavType.StringType })
                        ) { backStackEntry ->
                            val email = backStackEntry.arguments?.getString("email")
                            NextScreen(email = email)
                        }
                        composable("p5") {
                            showButtonBar.value = true
                            p5(navController = navController)
                            chooseItem.value = 5
                        }
                    }
                }
            }
        }
    }
}*/


class MainActivity : ComponentActivity() {
    @SuppressLint("UnusedMaterial3ScaffoldPaddingParameter", "NewApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

            val navController = rememberNavController()
            var showButtonBar by remember { mutableStateOf(true) }
            var chooseitem = remember { mutableStateOf(1) }
            var currentState by remember { mutableStateOf(0) }

            MyApplicationTheme {

                // Observe changes in the current route
                LaunchedEffect(navController) {
                    navController.addOnDestinationChangedListener { _, destination, _ ->
                        showButtonBar = when (destination.route) {
                            "SignIn" -> false
                            "SignUp" -> false
                            else -> true
                        }
                    }
                }

                Scaffold(
                    modifier = Modifier.fillMaxSize().background(MaterialTheme.colorScheme.background),
                    bottomBar = {
                        if (showButtonBar) {
                            bottomNavigation(navController, chooseitem)
                        }
                    }
                ) {

                    NavHost(navController = navController, startDestination = "SignIn") {

                        composable("SignIn") {
                            SignIn(navController = navController, viewModel = LoginViewModel(), )
                        }

                  /*      composable( "HomeScreen"yy
                      *//*      route = "HomeScreen/{email}",
                            arguments = listOf(navArgument("email") { type = NavType.StringType })*//*
                        ) { *//*backStackEntry ->*//*
                           *//* val email = backStackEntry.arguments?.getString("email")*//*

                            HomeScreen(navController*//*email = email*//*)
                            chooseitem.value = 1
                        }*/
                        composable("HomeScreen") {
                            HomeScreen(viewModel = GetListViewModel(), navController)
                            chooseitem.value = 1
                        }
                        //load Image from gallery
                        composable("PickImageFromGallery"){
                            PickImageFromGallery()

                        }
                       /* composable("ImageWithCaching"){
                            ImageWithCaching(imageUrl = String())
                        }*/

                        composable("SearchScreen"){
                            SearchScreen(navController)
                            chooseitem.value = 2
                        }

                        composable("ThreadScreen"){
                            ThreadScreen(navController, viewModel = MakeThreadViewModel())
                            chooseitem.value = 3
                        }

                        composable("LikeScreen"){
                            LikeScreen(navController)
                            chooseitem.value = 4
                        }

                        composable("ProfileScreen"){
                            ProfileScreen(navController)
                            chooseitem.value = 5
                        }
                        composable("Editprofile"){
                            Editprofile(
                                navController = navController
                            )

                        }


                        composable("SignUp") {
                            SignUp(
                                currentState = currentState,
                                onNext = {
                                    currentState = (currentState + 1) % 4
                                },
                                onPrevious = {
                                    currentState = if (currentState == 0) 3 else currentState - 1
                                },
                                navController = navController
                            )
                        }
                        composable("privacy") {
                            privacy(navController)
                        }

                        composable("setting") {
                            setting(navController)
                        }

                        composable("p3") {
                            p3(navController = navController)
                            chooseitem.value = 3
                        }


                        composable("p5") {
                            p5(navController = navController)
                            chooseitem.value = 5
                        }
                    }
                }

            }
        }
    }

   override fun onResume() {
           super.onResume()
           when (this.resources?.configuration?.uiMode?.and(Configuration.UI_MODE_NIGHT_MASK)) {
               Configuration.UI_MODE_NIGHT_NO -> {
                   window.statusBarColor = resources.getColor(R.color.white, null)
               }
               else -> {
                   window.statusBarColor = resources.getColor(R.color.black, null)
               }
           }
       }
}

/*

@Composable
fun NextScreen(email: String?) {
    Column(
        Modifier
            .fillMaxSize()
            .padding(16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        val ctx = LocalContext.current
        //Toast.makeText(ctx, "This is a Sample Toast", Toast.LENGTH_LONG).show()
        Text(text = "Welcome, $email", fontSize = 24.sp, fontWeight = FontWeight.Bold)
    }
}

*/
