package com.example.threadapp.Function

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.example.threadapp.Compose.getToken
import com.example.threadapp.R
import com.example.threadapp.viewmodel.ProfileViewModel


@Composable
fun Profile(modifier: Modifier = Modifier, viewModel:ProfileViewModel, navController: NavController) {

    val token = getToken(LocalContext.current) as String
    val profileRes by viewModel.profile.observeAsState()


    LaunchedEffect(token) {
        if (token != null) {
            viewModel.profile(token)
        }
    }

    val name = profileRes?.body()?.data?.firstName
    val lastName = profileRes?.body()?.data?.lastName
    val userName = profileRes?.body()?.data?.username
    val bio = profileRes?.body()?.data?.bio



    Column {
        Row(
            Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.Bottom
        ) {

            Column {
                if (name != null) {
                    Text(
                        text = name+lastName,
                        fontSize = 16.sp,
                        color = MaterialTheme.colorScheme.onPrimary,
                        fontWeight = FontWeight.Thin
                    )
                }
                if (userName != null) {
                    Text(
                        text = userName,
                        fontSize = 13.sp,
                        color = MaterialTheme.colorScheme.onPrimary,
                        fontWeight = FontWeight.Thin
                    )
                }
                if (bio != null) {
                    Text(
                        text = bio,
                        fontSize = 13.sp,
                        color = Color(0xFFB8B8B8),
                        fontWeight = FontWeight.Thin
                    )
                }

            }

            Box(
                Modifier
                    .size(70.dp)
                    .clip(shape = RoundedCornerShape(35.dp))
            ) {
                androidx.compose.foundation.Image(
                    painter = painterResource(
                        id = R.drawable.manprofile
                    ),
                    contentDescription = null,
                    contentScale = ContentScale.Crop,

                    )
            }



        }
        Spacer(modifier = Modifier.height(20.dp))

        Row(Modifier.fillMaxSize(),horizontalArrangement = Arrangement.SpaceEvenly){

            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .clip(shape = RoundedCornerShape(12.dp))
                    .clickable {navController.navigate("EditProfile") }
                    .background(Color.Transparent)
                    .border(1.dp, Color(0xFF2F2F2F), RoundedCornerShape(12.dp))
                    .width(176.dp)
                    .height(38.dp)
                    .padding(start = 11.dp, end = 11.dp)
            ) {
                Text(
                    text = "Edit Profile",
                    fontSize = 13.sp,
                    color = MaterialTheme.colorScheme.onPrimary
                )
            }

            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .clip(shape = RoundedCornerShape(12.dp))
                    .clickable { }
                    .background(Color.Transparent)
                    .border(1.dp, Color(0xFF2F2F2F), RoundedCornerShape(12.dp))
                    .width(176.dp)
                    .height(38.dp)
                    .padding(start = 11.dp, end = 11.dp)
            ) {
                Text(
                    text = "Share Profile",
                    fontSize = 13.sp,
                    color = MaterialTheme.colorScheme.onPrimary
                )
            }

        }

        Spacer(modifier = Modifier.height(30.dp))

        Row(Modifier.fillMaxWidth()) {

            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .clip(shape = RoundedCornerShape(12.dp))
                    .clickable { }
                    .background(Color.Transparent)
                    .border(1.dp, Color(0xFF2F2F2F), RoundedCornerShape(12.dp))
                    .fillMaxWidth(0.5f)
                    .height(38.dp)
                    .padding(start = 11.dp, end = 11.dp)
            ) {
                Text(
                    text = "Share Profile",
                    fontSize = 13.sp,
                    color = MaterialTheme.colorScheme.onPrimary
                )
            }

            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .clip(shape = RoundedCornerShape(12.dp))
                    .clickable { }
                    .background(Color.Transparent)
                    .border(1.dp, Color(0xFF2F2F2F), RoundedCornerShape(12.dp))
                    .fillMaxWidth()
                    .height(38.dp)
                    .padding(start = 11.dp, end = 11.dp)
            ) {
                Text(
                    text = "Share Profile",
                    fontSize = 13.sp,
                    color = MaterialTheme.colorScheme.onPrimary
                )
            }
        }
    }
}