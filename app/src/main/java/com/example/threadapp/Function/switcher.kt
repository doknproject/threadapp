package com.example.threadapp.Function

import androidx.compose.material3.Switch
import androidx.compose.material3.SwitchDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color


@Composable
fun switcher1(modifier: Modifier = Modifier) {
    var checked1 by remember { mutableStateOf(false) }

    Switch(
        checked = checked1, onCheckedChange ={ checked1 = it },
        colors = SwitchDefaults.colors(
            checkedIconColor = Color.White,
            checkedThumbColor = Color.Blue,
            uncheckedThumbColor = Color.Blue,
            checkedTrackColor = Color.Gray    // vaghti tick bokhore daron por mishe
        )
    )
}