package com.example.threadapp.Function

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.compose.rememberNavController
import com.example.threadapp.R

@Composable
fun Follower(modifier: Modifier = Modifier) {
    
    var Follow by remember{
        mutableStateOf(false)
    }

    Column(Modifier.fillMaxSize()) {
        Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Row(verticalAlignment = Alignment.CenterVertically) {

                Box(
                    Modifier
                        .size(46.dp)
                        .clip(shape = RoundedCornerShape(22.dp))
                ) {
                    androidx.compose.foundation.Image(
                        painter = painterResource(
                            id = R.drawable.manprofile
                        ),
                        contentDescription = null,
                        contentScale = ContentScale.Crop,

                        )
                }
                Spacer(modifier = Modifier.width(20.dp))

                Column {
                    Row(verticalAlignment = Alignment.CenterVertically) {

                        Text(
                            text = "Arman Karimian",
                            fontSize = 16.sp,
                            color = MaterialTheme.colorScheme.onPrimary

                        )
                        Spacer(modifier = Modifier.width(10.dp))

                        androidx.compose.foundation.Image(
                            painter = painterResource(
                                id = R.drawable.verrified
                            ),
                            contentDescription = null,
                            contentScale = ContentScale.Crop,

                            )

                    }

                    Text(
                        text = "Rmando",
                        fontSize = 13.sp,
                        color = Color(0xFFB8B8B8),
                        fontWeight = FontWeight.Thin
                    )

                }

            }
            
           Box(
               modifier
                   .width(113.dp)
                   .height(35.dp)
                   .clip(RoundedCornerShape(20.dp))
                   .background(if (Follow) Color(0xFF3F3F3F) else Color(0xFF0070FA))
                   .border(1.dp ,Color(0xFF3F3F3F), RoundedCornerShape(20.dp))
                   .clickable { Follow = !Follow },
               contentAlignment = Alignment.Center,

           ) {
               Text(text = "Follow" , fontSize = 16.sp)

           }
        }
        Spacer(modifier = Modifier.height(10.dp))


            Row(Modifier.fillMaxWidth()) {
                Spacer(modifier = Modifier.width(66.dp))

                Column {

                    Text(
                        text = "35 followers",
                        fontSize = 16.sp,
                        color = MaterialTheme.colorScheme.onPrimary,
                        fontWeight = FontWeight.Thin
                    )
                    Spacer(modifier = Modifier.height(10.dp))

                    Divider(
                        modifier = Modifier
                            .fillMaxWidth(),
                        color = Color(0xFF3F3F3F)
                    )
                }

            }
    }


}