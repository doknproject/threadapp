package com.example.threadapp.profileData

data class ProfileData(
    val status: Int,
    val data: UserData
)

data class UserData(
    val id: String,
    val email: String,
    val privateAccount: Boolean,
    val username: String,
    val firstName: String,
    val lastName: String,
    val bio: String,
    val profileImage: String,
    val birthday: String?, // Nullable String for birthday
    val verified: Boolean,
    val posts: List<Any>, // Assuming posts is a list of objects, replace Any with actual type if needed
    val follower: List<Any>, // Assuming follower is a list of objects, replace Any with actual type if needed
    val following: List<Any> // Assuming following is a list of objects, replace Any with actual type if needed
)