package com.example.threadapp

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.systemBarsPadding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.graphics.Color.Companion.Blue
import androidx.compose.ui.graphics.Color.Companion.Transparent
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import kotlin.math.sign

@Composable
fun p3(navController: NavController) {

    var textvalue by remember {
        mutableStateOf("")
    }

 Column(
     Modifier
         .systemBarsPadding()
         .fillMaxSize()
         .padding(start = 10.dp, top = 10.dp, end = 10.dp)

 ) {

     Row(
         Modifier.fillMaxWidth(),
         verticalAlignment = Alignment.CenterVertically
     ) {

         IconButton(onClick = { }) {

             Icon(
                 painter = painterResource(id = R.drawable.lefticon),
                 contentDescription = null,
                 tint = Black,
                 modifier = Modifier.size(24.dp)
             )
         }

             TextField(
                 value = textvalue, onValueChange = { newtext ->
                     textvalue = newtext
                 },
                  singleLine = true, maxLines = 1,
                 colors = TextFieldDefaults.colors(
                     unfocusedIndicatorColor = Color.Transparent,
                     focusedIndicatorColor = Color.Transparent,
                     focusedContainerColor = Blue,
                     unfocusedContainerColor = Color.Gray,
                     focusedLabelColor = Black,
                     unfocusedLabelColor = Black,
                     focusedTextColor = Color.Black,
                     unfocusedTextColor = Black
                 ),
                 modifier = Modifier
                     .fillMaxWidth()
                     .height(24.dp),
                 label = {
                     Text(
                         text = "Search",
                         color = Color(0xFFA0A0A0)
                     )
                 },
                 leadingIcon = {

                     Icon(
                         // if i want use from resource i can use image vector or anything else
                         //and then i just have to use .vectorResource
                         imageVector = ImageVector.vectorResource(id = R.drawable.search),
                         contentDescription = null,
                         Modifier.size(20.dp),
                         tint = Color(0xFFA0A0A0)
                     )
                 },
                 shape = RoundedCornerShape(10.dp)
             )
         }
     Spacer(modifier = Modifier.height(10.dp))

            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(start = 10.dp)
                ,
                horizontalArrangement = Arrangement.SpaceBetween,

            ){
                Text(
                    text = " Recent ",
                    color = Color.Black,
                    fontSize = 16.sp,
                )
                Text(
                    text = " Clear ",
                    color = Color.Black,
                    fontSize = 16.sp,
                )

            }

     }

     }