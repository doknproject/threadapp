package com.example.threadapp.LogInData

data class SignInRes(
    val access_token : String,
    val user : User,
    val message : String,
    val statusCode : Int
)