package com.example.threadapp.LogInData

data class User(
    val id: String,
    val email: String,
    val privateAccount: Boolean,
    val username: String,
    val firstName: String,
    val lastName: String,
    val bio: String? = null,
    val profileImage: String,
    val birthday: String? = null,
    val verified: Boolean
)