package com.example.threadapp.LogInData

data class SignInReq(
    val username: String,
    val password: String
)